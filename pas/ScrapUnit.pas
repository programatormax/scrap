unit ScrapUnit;

interface


uses

  Windows,
  SysUtils,
  Classes,
  DateUtils,
  curl_h,
  IdHTTP,
  IdSSLOpenSSL,
  IdSSL,
  Web.HTTPApp,
  ShellApi,
  shlobj,
  SuperObject,
  MySql;

const

  HASH_MAX_COUNT = 16;

type

  TQuery = class
  private
    FActive: Boolean;
    procedure Reset;
    procedure SetActive(Act: Boolean);
    function GetHash(V: AnsiString): AnsiString;
    function GetUrlObject(Url: String): ISuperObject;
  protected
    procedure DoReset; virtual; abstract;
    function DoActivate: Boolean; virtual; abstract;
    function DoDeactivate: Boolean; virtual; abstract;
    function DoGet(Url: ISuperObject): ISuperObject; virtual; abstract;
    function DoPost(Url: ISuperObject; Body: ISuperObject): ISuperObject; virtual; abstract;
  public
    property Active: Boolean read FActive write SetActive;
    function Get(Url: AnsiString): ISuperObject;
    function Post(Url: AnsiString; Body: ISuperObject): ISuperObject;
    constructor Create;
    destructor Destroy; override;
  end;

  TScrap = class (TQuery);

  TPush = class (TQuery);

  TScrapInvestment = class (TScrap)
  private
    FStrings: TStringList;
    FStringsEx: TStringList;
    function CopyArray(dest: TSuperArray; source: TSuperArray): Boolean;
    function ParsingEconomicCalendar(Url: AnsiString; DateFrom: AnsiString; DateTo: AnsiString; HAvoid: Boolean; Importance: Integer): ISuperObject;
    function ParsingCentralBanksRate(Url: AnsiString; var Cur: AnsiString; var Lt: UInt64): ISuperObject;
    function ParsingCentralBanksRateEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;

    function ParsingFinancialSummaryAnnual(Url: AnsiString): AnsiString;
    function ParsingFinancialSummary(Url1: AnsiString; Url2: AnsiString): ISuperObject;
    function ParsingSummaryIncomeStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
    function ParsingSummaryBalanceSheet(Url: AnsiString; Cur: AnsiString): ISuperObject;
    function ParsingBalanceSheet(Url: AnsiString; Cur: AnsiString): ISuperObject;
    function ParsingIncomeStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
    function ParsingSummaryCashFlowStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
    function ParsingDividends(Url: AnsiString; var Lt: UInt64): ISuperObject;
    function ParsingDividendsEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;
    function ParsingLatestRelease(Url: AnsiString): AnsiString;
    function ParsingRation(DT: AnsiString; Url: AnsiString): ISuperObject;
    function ParsingEarningEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;
    function ParsingEarning(Url: AnsiString; var lt: UInt64): ISuperObject;
    function ParsingCashflow(Url: AnsiString; Cur: AnsiString): ISuperObject;
  protected
    procedure DoReset; override;
    function DoActivate: Boolean; override;
    function DoDeactivate: Boolean; override;
    function DoGet(Url: ISuperObject): ISuperObject; override;
    function DoPost(Url: ISuperObject; Body: ISuperObject): ISuperObject; override;
  end;

  TPushMysqlDatabase = class (TPush)
  private
    FhMySql: PMYSQL;
    Fhstmt: PMYSQL_STMT;
    FhRes: PMYSQL_RES;
    FhRow: PMYSQL_ROW;
    FAddress: AnsiString;
    FPort: Word;
    FLogin: AnsiString;
    FPassword: AnsiString;
    FScheme: AnsiString;
    FHashlist: TStringList;
    FHashControl: Boolean;
    function Correct(V: AnsiString): AnsiString;
    function CorrectD(V: AnsiString): AnsiString;
    procedure SetAddress(V: AnsiString);
    procedure SetPort(V: Word);
    procedure SetLogin(V: AnsiString);
    procedure SetPassword(V: AnsiString);
    procedure SetScheme(V: AnsiString);
    procedure SetControl(V: Boolean);
    function GetFieldIncomestatement(Name: AnsiString): UInt64;
    function GetFieldBalanceSheet(Name: AnsiString): UInt64;
    function GetFieldRation(Name: AnsiString): UInt64;
    function GetFieldCashflow(Name: AnsiString): UInt64;
    function GetFieldDivident(TP: AnsiString; Name: AnsiString): UInt64;
  protected
    procedure DoReset; override;
    function DoActivate: Boolean; override;
    function DoDeactivate: Boolean; override;
    function DoGet(Url: ISuperObject): ISuperObject; override;
    function DoPost(Url: ISuperObject; Body: ISuperObject): ISuperObject; override;
  public
    property Active;
    property Address: AnsiString read FAddress write SetAddress;
    property Port: Word read FPort write SetPort;
    property Login: AnsiString read FLogin write SetLogin;
    property Password: AnsiString read FPassword write SetPassword;
    property Scheme: AnsiString read FScheme write SetScheme;
    property Control: Boolean read FHashControl write SetControl;
  end;

  function HtmlGet(Url: AnsiString): AnsiString;
  function HttpGet(Url: AnsiString): AnsiString;
  function HttpPostQ(Url: AnsiString; PairId: Integer; timestamp: UInt64): AnsiString;
  function HttpPostB(Url: AnsiString; PairId: Integer; timestamp: UInt64): AnsiString;
  function HttpPost(Url: AnsiString; FromDate: AnsiString; ToDate: AnsiString; Importance: Integer): AnsiString;

implementation

{functions}

//MessageBox
procedure ShowMessage(Str: AnsiString);
begin
  MessageBoxA(0,PAnsiChar(Str),'Message',0);
end;

//Convert
function StrToUnix(Str: AnsiString): UInt64;
var F: TFormatSettings;
begin
  GetLocaleFormatSettings(LOCALE_SYSTEM_DEFAULT, F);
  F.TimeSeparator:=':';
  F.DateSeparator:='-';
  F.ShortDateFormat:='YYYY-MM-DD';
  Result:=DateTimeToUnix(StrToDateTime(Str,F));
end;

function UnixToStr(N: UInt64): String;
var R: String;
begin
  DateTimeToString(R,'YYYY-MM-DD HH:MM:SS',UnixToDateTime(N));
  Result:=R;
end;

//2022/26/03 00:00:00
function CorrectMySql(Str: AnsiString): AnsiString;
begin
  if Length(Str)=19 then Result:=Copy(Str,1,5)+Copy(Str,9,2)+'/'+Copy(Str,6,2)+' '+Copy(Str,12,8);
  Result:=Trim(StringReplace(Result,'/','-',[rfReplaceAll, rfIgnoreCase]));
end;

function CorrectMySqlShort(Str: AnsiString): AnsiString;
begin
  Result:=Copy(Str,1,2);
  Delete(Str,1,2);
  Result:=Str+'-'+Result+'-01 00:00:00';
end;

function FormatMySqlUnix(Str: AnsiString): AnsiString;
begin
  if Str<>'' then
  begin
    Result:=FormatDateTime('yyyy-mm-dd hh:nn:ss',UnixToDateTime(StrToInt(Trim(Str))));
  end
  else
  begin
    Result:=Str;
  end;
end;

function FormatMySqlNow: AnsiString;
begin
  Result:=FormatDateTime('yyyy-mm-dd hh:nn:ss',Now);
end;

function OnRecvData(Buffer: PChar; Size, nItems: longword; UserData: pointer): integer;  cdecl;
begin
  Result:=Size*nItems;
  if Result>0 then TMemoryStream(UserData).Write(Buffer^,Result);
end;

function HtmlGet(Url: AnsiString): AnsiString;
var FhCurl: pCurl;
    FhCurlCode: CURLcode;
    FhCurlStream: TMemoryStream;
    FParams: pcurl_slist;
    Opt: CURLoption;
begin
  if Url<>'' then
  begin
    FhCurl:=curl_easy_init;
    if FhCurl<>nil then
    begin
      Opt:=CURLoption.CURLOPT_WRITEFUNCTION;
      FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_WRITEFUNCTION, @OnRecvData);
      if FhCurlCode=CURLE_OK then
      begin
        FhCurlStream:=TMemoryStream.Create;
        //Opt:=CURLoption.CURLOPT_WRITEDATA;
        FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_WRITEDATA,Pointer(FhCurlStream));
        if FhCurlCode=CURLE_OK then
        begin
          FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_URL, PAnsiChar(Url));
          if FhCurlCode=CURLE_OK then
          begin
            //curl_easy_setopt(FhCurl, CURLOPT_CAINFO, PAnsiChar(ExtractFilePath(paramstr(0))+'cacet.pem'));
            FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_SSL_VERIFYPEER,false);
            if FhCurlCode=CURLE_OK  then
            begin
              FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,60);
              if FhCurlCode=CURLE_OK  then
              begin
                FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,60);
                if FhCurlCode=CURLE_OK  then
                begin
                  FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_HTTPGET,1);
                  if FhCurlCode=CURLE_OK  then
                  begin
                    FParams:=nil;
                    FParams:=curl_slist_append(FParams,'Content-type: application/x-www-form-urlencoded');
                    FParams:=curl_slist_append(FParams,'User-Agent: Mozilla/4.0');
                    FParams:=curl_slist_append(FParams,'Accept: text/html, image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*');
                    FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
                    if FhCurlCode=CURLE_OK  then
                    begin
                      FhCurlCode:=curl_easy_perform(FhCurl);
                      if FhCurlCode=CURLE_OK  then
                      begin
                        if FhCurlStream.Size>0 then
                        begin
                          Result:=PAnsiChar(FhCurlStream.Memory);
                        end
                        else
                        begin
                          Result:='Curl no data';
                        end;
                      end
                      else
                      begin
                        Result:='Curl QUERY '+IntToStr(Integer(FhCurlCode));
                      end;
                    end
                    else
                    begin
                      Result:='Curl CURLOPT_HTTPHEADER '+IntToStr(Integer(FhCurlCode));
                    end;
                    curl_slist_free_all(FParams);
                    FParams:=nil;
                  end
                  else
                  begin
                    Result:='Curl CURLOPT_HTTPGET '+IntToStr(Integer(FhCurlCode));
                  end;
                end
                else
                begin
                  Result:='Curl CURLOPT_TIMEOUT '+IntToStr(Integer(FhCurlCode));
                end;
              end
              else
              begin
                Result:='Curl CURLOPT_CONNECTTIMEOUT '+IntToStr(Integer(FhCurlCode));
              end;
            end
            else
            begin
              Result:='Curl CURLOPT_SSL_VERIFYPEER '+IntToStr(Integer(FhCurlCode));
            end;
          end
          else
          begin
            Result:='Curl CURLOPT_URL '+IntToStr(Integer(FhCurlCode));
          end;
        end
        else
        begin
          Result:='Curl CURLOPT_WRITEDATA '+IntToStr(Integer(FhCurlCode));
        end;
        FhCurlStream.Destroy;
        FhCurlStream:=nil;
      end
      else
      begin
        Result:='Curl CURLOPT_WRITEFUNCTION '+IntToStr(Integer(FhCurlCode));
      end;
      curl_easy_cleanup(FhCurl);
      FhCurl:=nil;
    end
    else
    begin
      Result:='Curl initialization fault';
    end;
  end
  else
  begin
    Result:='Url not found';
  end;
end;

function HttpGet(Url: AnsiString): AnsiString;
var
  HTTP: TIdHTTP;
begin
  Result:='';
  HTTP:=TIdHTTP.Create;
  HTTP.Request.Accept:='text/html, image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*';
  //HTTP.Request.AcceptEncoding := 'gzip, deflate';
  HTTP.Request.BasicAuthentication:=False;
  HTTP.Request.UserAgent:='Mozilla/4.0';
  HTTP.Request.ContentType:='application/x-www-form-urlencoded';
  try
    if not Assigned(Http.IOHandler) then
    begin
      HTTP.IOHandler:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      TIdSSLIOHandlerSocketOpenSSL(HTTP.IOHandler).SSLOptions.SSLVersions:=[sslvTLSv1_2];
    end;
    //HTTP.Request.BasicAuthentication := False;
    //HTTP.Request.Username := 'sk_test_CpkBxhx9gcmNYYQTZIXU43Bv';
    try
      Result:=HTTP.Get(Url);
    except
      on E: EIdHTTPProtocolException do
      begin
        Result:=E.Message;
      end;
      on E: Exception do
      begin
        Result:=E.Message;
      end;
    end;
  finally
    HTTP.Free;
  end;
end;

function HttpPostB(Url: AnsiString; PairId: Integer; timestamp: UInt64): AnsiString;
var
  HTTP: TIdHTTP;
  Post: TStringList;
  S: AnsiString;
  dt: TDateTime;

  function IntToStrA(i: UInt64): AnsiString;
  begin
    if i>9 then Result:=IntToStr(i) else Result:='0'+IntToStr(i);
  end;

begin
  Url:='https://www.investing.com/economic-calendar/more-history';
  dt:=UnixToDateTime(timestamp);
  Result:='';
  HTTP:=TIdHTTP.Create;
  Post:=TStringList.Create;
  HTTP.Request.Accept:='application/json, text/javascript, */*; q=0.01';
  HTTP.Request.BasicAuthentication:=False;
  HTTP.Request.UserAgent:='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36';
  HTTP.Request.ContentType:='application/x-www-form-urlencoded; charset=UTF-8';
  HTTP.Request.CustomHeaders.Values['x-requested-with']:='XMLHttpRequest';
  HTTP.Request.CustomHeaders.Values['origin']:='https://www.investing.com';
  HTTP.Request.Pragma:='no-cache';
  Post.Values['eventID']:='436027';
  Post.Values['event_attr_ID']:=IntToStr(PairId);
  //Post.Values['event_timestamp']:='2022-05-04 18:00:00';
  Post.Values['event_timestamp']:=IntToStrA(YearOf(dt))+'-'+IntToStrA(MonthOf(dt))+'-'+IntToStrA(DayOf(dt))+' '+IntToStrA(HourOf(dt))+':'+IntToStrA(MinuteOf(dt))+':'+IntToStrA(SecondOf(dt));
  Post.Values['is_speech']:='0';
  try
    if not Assigned(Http.IOHandler) then
    begin
      HTTP.IOHandler:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      TIdSSLIOHandlerSocketOpenSSL(HTTP.IOHandler).SSLOptions.SSLVersions:=[sslvTLSv1_2];
    end;
    try
      Result:=HTTP.Post(Url, Post);
      Result:=StringReplace(Result,'\n',#10,[rfReplaceAll]);
      Result:=StringReplace(Result,'\r',#13,[rfReplaceAll]);
      Result:=StringReplace(Result,'\t','',[rfReplaceAll]);
      Result:=StringReplace(Result,'\/','/',[rfReplaceAll]);
      Result:=StringReplace(Result,'=\','=',[rfReplaceAll]);
      Result:=StringReplace(Result,'\"','"',[rfReplaceAll]);
      Result:=StringReplace(Result,'  ','',[rfReplaceAll]);
      Result:=StringReplace(Result,'</td><','</td>'#13#10+'<',[rfReplaceAll]);
    except
      on E: EIdHTTPProtocolException do
      begin
        Result:=E.Message;
      end;
      on E: Exception do
      begin
        Result:=E.Message;
      end;
    end;
  finally
    HTTP.Free;
    Post.Free;
  end;
end;


function HttpPostZ(Url: AnsiString; PairId: Integer; timestamp: UInt64): AnsiString;
var
  HTTP: TIdHTTP;
  Post: TStringList;
  S: AnsiString;
  dt: TDateTime;

  function IntToStrA(i: UInt64): AnsiString;
  begin
    if i>9 then Result:=IntToStr(i) else Result:='0'+IntToStr(i);
  end;

begin
  Result:='';
  dt:=UnixToDateTime(timestamp);
  HTTP:=TIdHTTP.Create;
  Post:=TStringList.Create;
  HTTP.Request.Accept:='application/json, text/javascript, */*; q=0.01';
  HTTP.Request.BasicAuthentication:=False;
  HTTP.Request.UserAgent:='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36';
  HTTP.Request.ContentType:='application/x-www-form-urlencoded; charset=UTF-8';
  HTTP.Request.CustomHeaders.Values['x-requested-with']:='XMLHttpRequest';
  HTTP.Request.CustomHeaders.Values['origin']:='https://www.investing.com';
  HTTP.Request.Pragma:='no-cache';
  Post.Values['pairID']:=IntToStr(PairId);
  Post.Values['last_timestamp']:=IntToStrA(YearOf(dt))+'-'+IntToStrA(MonthOf(dt))+'-'+IntToStrA(DayOf(dt));
  try
    if not Assigned(Http.IOHandler) then
    begin
      HTTP.IOHandler:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      TIdSSLIOHandlerSocketOpenSSL(HTTP.IOHandler).SSLOptions.SSLVersions:=[sslvTLSv1_2];
    end;
    try
      Result:=HTTP.Post(Url, Post);
      Result:=StringReplace(Result,'\n',#10,[rfReplaceAll]);
      Result:=StringReplace(Result,'\r',#13,[rfReplaceAll]);
      Result:=StringReplace(Result,'\t','',[rfReplaceAll]);
      Result:=StringReplace(Result,'\/','/',[rfReplaceAll]);
      Result:=StringReplace(Result,'=\','=',[rfReplaceAll]);
      Result:=StringReplace(Result,'\"','"',[rfReplaceAll]);
      Result:=StringReplace(Result,'  ','',[rfReplaceAll]);
      Result:=StringReplace(Result,'</td><','</td>'#13#10+'<',[rfReplaceAll]);
    except
      on E: EIdHTTPProtocolException do
      begin
        Result:=E.Message;
      end;
      on E: Exception do
      begin
        Result:=E.Message;
      end;
    end;
  finally
    HTTP.Free;
    Post.Free;
  end;
end;

function HttpPostQ(Url: AnsiString; PairId: Integer; timestamp: UInt64): AnsiString;
var
  HTTP: TIdHTTP;
  Post: TStringList;
  S: AnsiString;
begin
  Url:='https://www.investing.com/equities/MoreDividendsHistory';
  Result:='';
  HTTP:=TIdHTTP.Create;
  Post:=TStringList.Create;
  HTTP.Request.Accept:='application/json, text/javascript, */*; q=0.01';
  HTTP.Request.BasicAuthentication:=False;
  HTTP.Request.UserAgent:='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36';
  HTTP.Request.ContentType:='application/x-www-form-urlencoded; charset=UTF-8';
  HTTP.Request.CustomHeaders.Values['x-requested-with']:='XMLHttpRequest';
  HTTP.Request.CustomHeaders.Values['origin']:='https://www.investing.com';
  HTTP.Request.Pragma:='no-cache';
  Post.Values['pairID']:=IntToStr(PairId);
  Post.Values['last_timestamp']:=IntToStr(timestamp);
  try
    if not Assigned(Http.IOHandler) then
    begin
      HTTP.IOHandler:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      TIdSSLIOHandlerSocketOpenSSL(HTTP.IOHandler).SSLOptions.SSLVersions:=[sslvTLSv1_2];
    end;
    try
      Result:=HTTP.Post(Url, Post);
      Result:=StringReplace(Result,'\n',#10,[rfReplaceAll]);
      Result:=StringReplace(Result,'\r',#13,[rfReplaceAll]);
      Result:=StringReplace(Result,'\t','',[rfReplaceAll]);
      Result:=StringReplace(Result,'\/','/',[rfReplaceAll]);
      Result:=StringReplace(Result,'=\','=',[rfReplaceAll]);
      Result:=StringReplace(Result,'\"','"',[rfReplaceAll]);
      Result:=StringReplace(Result,'  ','',[rfReplaceAll]);
      Result:=StringReplace(Result,'</td><','</td>'#13#10+'<',[rfReplaceAll]);
    except
      on E: EIdHTTPProtocolException do
      begin
        Result:=E.Message;
      end;
      on E: Exception do
      begin
        Result:=E.Message;
      end;
    end;
  finally
    HTTP.Free;
    Post.Free;
  end;
end;

function HttpPost(Url: AnsiString; FromDate: AnsiString; ToDate: AnsiString; Importance: Integer): AnsiString;
var
  HTTP: TIdHTTP;
  Post: TStringList;
  S: AnsiString;
begin
  Result:='';
  HTTP:=TIdHTTP.Create;
  Post:=TStringList.Create;
  HTTP.Request.Accept:='application/json, text/javascript, */*; q=0.01';
  HTTP.Request.BasicAuthentication:=False;
  HTTP.Request.UserAgent:='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36';
  HTTP.Request.ContentType:='application/x-www-form-urlencoded; charset=UTF-8';
  HTTP.Request.CustomHeaders.Values['x-requested-with']:='XMLHttpRequest';
  HTTP.Request.CustomHeaders.Values['origin']:='https://www.investing.com';
  HTTP.Request.Pragma:='no-cache';
  Post.Values['dateFrom']:=FromDate;
  Post.Values['dateTo']:=ToDate;
  Post.Values['timeZone']:='8';
  Post.Values['timeframe']:='';
  Post.Values['action']:='filter';
  Post.Values['lang']:='1';
  Post.Add('columns[]=exc_flags');
  Post.Add('columns[]=exc_currency');
  Post.Add('columns[]=exc_importance');
  Post.Add('columns[]=exc_actual');
  Post.Add('columns[]=exc_forecast');
  Post.Add('columns[]=exc_previous');
  case Importance of
    1:
    begin
      Post.Add('importance[]=1');
    end;
    2:
    begin
      Post.Add('importance[]=2');
    end;
    3:
    begin
      Post.Add('importance[]=2');
    end;
  else
    Post.Add('importance[]=1');
    Post.Add('importance[]=2');
    Post.Add('importance[]=3');
  end;
  Post.Add('category[]=_employment');
  Post.Add('category[]=_economicActivity');
  Post.Add('category[]=_inflation');
  Post.Add('category[]=_credit');
  Post.Add('category[]=_centralBanks');
  Post.Add('category[]=_confidenceIndex');
  Post.Add('category[]=_balance');
  Post.Add('category[]=_Bonds');
  try
    if not Assigned(Http.IOHandler) then
    begin
      HTTP.IOHandler:=TIdSSLIOHandlerSocketOpenSSL.Create(nil);
      TIdSSLIOHandlerSocketOpenSSL(HTTP.IOHandler).SSLOptions.SSLVersions:=[sslvTLSv1_2];
    end;
    try
      Result:=HTTP.Post(Url, Post);
      Result:=StringReplace(Result,'\n',#10,[rfReplaceAll]);
      Result:=StringReplace(Result,'\r',#13,[rfReplaceAll]);
      Result:=StringReplace(Result,'\t','',[rfReplaceAll]);
      Result:=StringReplace(Result,'\/','/',[rfReplaceAll]);
      Result:=StringReplace(Result,'=\','=',[rfReplaceAll]);
      Result:=StringReplace(Result,'\"','"',[rfReplaceAll]);
      Result:=StringReplace(Result,'  ','',[rfReplaceAll]);
      Result:=StringReplace(Result,'</td><','</td>'#13#10+'<',[rfReplaceAll]);
    except
      on E: EIdHTTPProtocolException do
      begin
        Result:=E.Message;
      end;
      on E: Exception do
      begin
        Result:=E.Message;
      end;
    end;
  finally
    HTTP.Free;
    Post.Free;
  end;
end;


function GetProgramData: AnsiString;
var
   FilePath: array [0..MAX_PATH] of AnsiChar;
begin
{$ifdef DEBUG}
  Result:=ExtractFilePath(ParamStr(0));
{$else}
  SHGetFolderPathA(0, CSIDL_COMMON_APPDATA, 0, 0, FilePath);
  Result:=FilePath;
  Result:=Result+'\cache\';
{$endif}
end;

function FileGet(Url: AnsiString): AnsiString;
var
  FileStream : TFileStream;
  Path: AnsiString;
begin
  Result:='';
  Path:=GetProgramData+Url;
  if FileExists(Path)=True then
  begin
    //writeln('Exists: '+Url);
    FileStream:= TFileStream.Create(Path, fmOpenRead or fmShareDenyWrite);
    try
      if FileStream.Size>0 then
      begin
        SetLength(Result, FileStream.Size);
        FileStream.Read(Pointer(Result)^, FileStream.Size);
      end;
    finally
      FileStream.Destroy;
      FileStream:=nil;
    end;
  end
  else
  begin
    //writeln('Not Exists: '+Url);
  end;
end;

function RestApiGet(Url: AnsiString): ISuperObject;
var FhCurl: pCurl;
    FhCurlCode: CURLcode;
    FhCurlStream: TMemoryStream;
    FParams: pcurl_slist;
    Opt: CURLoption;
begin
  if Url<>'' then
  begin
    FhCurl:=curl_easy_init;
    if FhCurl<>nil then
    begin
      Opt:=CURLoption.CURLOPT_WRITEFUNCTION;
      FhCurlCode:=curl_easy_setopt(FhCurl,Opt, @OnRecvData);
      if FhCurlCode=CURLE_OK then
      begin
        FhCurlStream:=TMemoryStream.Create;
        FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_WRITEDATA,Pointer(FhCurlStream));
        if FhCurlCode=CURLE_OK then
        begin
          FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_URL, PAnsiChar(Url));
          if FhCurlCode=CURLE_OK then
          begin
            //curl_easy_setopt(FhCurl, CURLOPT_CAINFO, PAnsiChar(ExtractFilePath(paramstr(0))+'cacet.pem'));
            FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_SSL_VERIFYPEER,false);
            if FhCurlCode=CURLE_OK  then
            begin
              FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_CONNECTTIMEOUT,60);
              if FhCurlCode=CURLE_OK  then
              begin
                FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_TIMEOUT,60);
                if FhCurlCode=CURLE_OK  then
                begin
                  FhCurlCode:=curl_easy_setopt(FhCurl, CURLOPT_HTTPGET,1);
                  if FhCurlCode=CURLE_OK  then
                  begin
                    FParams:=nil;
                    FParams:=curl_slist_append(FParams,'Content-type: application/json');
                    FParams:=curl_slist_append(FParams,'Accept: application/json');
                    FhCurlCode:=curl_easy_setopt(FhCurl,CURLOPT_HTTPHEADER,FParams);
                    if FhCurlCode=CURLE_OK  then
                    begin
                      FhCurlCode:=curl_easy_perform(FhCurl);
                      if FhCurlCode=CURLE_OK  then
                      begin
                        if FhCurlStream.Size>0 then
                        begin
                          Result:=SO(PAnsiChar(FhCurlStream.Memory));
                        end
                        else
                        begin
                          Result:=SO;
                          Result.B['response.result']:=False;
                          Result.I['response.code']:=400;
                          Result.S['response.status']:='Bad request';
                          Result.S['response.description']:='Curl no data';
                        end;
                      end
                      else
                      begin
                        Result:=SO;
                        Result.B['response.result']:=False;
                        Result.I['response.code']:=400;
                        Result.S['response.status']:='Bad request';
                        Result.S['response.description']:='Curl QUERY '+IntToStr(Integer(FhCurlCode));
                      end;
                    end
                    else
                    begin
                      Result:=SO;
                      Result.B['response.result']:=False;
                      Result.I['response.code']:=400;
                      Result.S['response.status']:='Bad request';
                      Result.S['response.description']:='Curl CURLOPT_HTTPHEADER '+IntToStr(Integer(FhCurlCode));
                    end;
                    curl_slist_free_all(FParams);
                    FParams:=nil;
                  end
                  else
                  begin
                    Result:=SO;
                    Result.B['response.result']:=False;
                    Result.I['response.code']:=400;
                    Result.S['response.status']:='Bad request';
                    Result.S['response.description']:='Curl CURLOPT_HTTPGET '+IntToStr(Integer(FhCurlCode));
                  end;
                end
                else
                begin
                  Result:=SO;
                  Result.B['response.result']:=False;
                  Result.I['response.code']:=400;
                  Result.S['response.status']:='Bad request';
                  Result.S['response.description']:='Curl CURLOPT_TIMEOUT '+IntToStr(Integer(FhCurlCode));
                end;
              end
              else
              begin
                Result:=SO;
                Result.B['response.result']:=False;
                Result.I['response.code']:=400;
                Result.S['response.status']:='Bad request';
                Result.S['response.description']:='Curl CURLOPT_CONNECTTIMEOUT '+IntToStr(Integer(FhCurlCode));
              end;
            end
            else
            begin
              Result:=SO;
              Result.B['response.result']:=False;
              Result.I['response.code']:=400;
              Result.S['response.status']:='Bad request';
              Result.S['response.description']:='Curl CURLOPT_SSL_VERIFYPEER '+IntToStr(Integer(FhCurlCode));
            end;
          end
          else
          begin
            Result:=SO;
            Result.B['response.result']:=False;
            Result.I['response.code']:=400;
            Result.S['response.status']:='Bad request';
            Result.S['response.description']:='Curl CURLOPT_URL '+IntToStr(Integer(FhCurlCode));
          end;
        end
        else
        begin
          Result:=SO;
          Result.B['response.result']:=False;
          Result.I['response.code']:=400;
          Result.S['response.status']:='Bad request';
          Result.S['response.description']:='Curl CURLOPT_WRITEDATA '+IntToStr(Integer(FhCurlCode));
        end;
        FhCurlStream.Destroy;
        FhCurlStream:=nil;
      end
      else
      begin
        Result:=SO;
        Result.B['response.result']:=False;
        Result.I['response.code']:=400;
        Result.S['response.status']:='Bad request';
        Result.S['response.description']:='Curl CURLOPT_WRITEFUNCTION '+IntToStr(Integer(FhCurlCode));
      end;
      curl_easy_cleanup(FhCurl);
      FhCurl:=nil;
    end
    else
    begin
      Result:=SO;
      Result.B['response.result']:=False;
      Result.I['response.code']:=400;
      Result.S['response.status']:='Bad request';
      Result.S['response.description']:='Curl initialization fault';
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['response.result']:=False;
    Result.I['response.code']:=400;
    Result.S['response.status']:='Bad request';
    Result.S['response.description']:='Url not found';
  end;
end;

function ParamGet(Par: AnsiString; Str: AnsiString): AnsiString;
var i,m,p: UInt64;
begin
  Result:='';
  i:=pos(par+'=',Str);
  if i>0 then
  begin
    m:=pos('"',Str,i);
    if m>i then
    begin
      p:=m+1;
      m:=pos('"',Str,p);
      if m>p then Result:=StringReplace(Trim(Copy(Str,p,m-p)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
    end;
  end;
end;

function ParamDef(Str: AnsiString): AnsiString;
var i,m,p: UInt64;
begin
  Result:='';
  i:=pos('>',Str,1);
  if i>0 then
  begin
    i:=i+1;
    p:=pos('<',Str,i);
    if p>i then
    begin
      Result:=StringReplace(Trim(Copy(Str,i,p-i)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
      Result:=StringReplace(Result,'/','',[rfReplaceAll, rfIgnoreCase]);
      if (Result='--') or (Result='-') then Result:='';
    end;
  end;
end;

function ParamSkb(Str: AnsiString): AnsiString;
var i,m,p: UInt64;
begin
  Result:='';
  i:=pos('(',Str,1);
  if i>0 then
  begin
    i:=i+1;
    p:=pos(')',Str,i);
    if p>i then Result:=StringReplace(Trim(Copy(Str,i,p-i)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
  end;
end;

function ParamDefA(Str: AnsiString): AnsiString;
var i,m,p: UInt64;
begin
  Result:='';
  i:=pos('n>',Str,1);
  if i>0 then
  begin
    i:=i+2;
    p:=pos('<',Str,i);
    if p>i then Result:=StringReplace(Trim(Copy(Str,i,p-i)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
    if (Length(Result)=1) and (Result[1]='-') then Result:='';
  end;
end;

function ParamDefB(Str: AnsiString): AnsiString;
var i,m,p: UInt64;
begin
  Result:='';
  i:=pos('ld">',Str,1);
  if i>0 then
  begin
    i:=i+4;
    p:=pos('<',Str,i);
    if p>i then
    begin
      Result:=StringReplace(Trim(Copy(Str,i,p-i)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
      if (Length(Result)=1) and (Result[1]='-') then Result:='';
    end;
  end;
end;

function ParamDefE(Q: AnsiString; Str: AnsiString): AnsiString;
var i,m,l,p: UInt64;
begin
  Result:='';
  l:=Length(Q);
  i:=pos(Q,Str,1);
  if i>0 then
  begin
    i:=i+l;
    p:=pos('<',Str,i);
    if p>i then Result:=StringReplace(Trim(Copy(Str,i,p-i)),'&nbsp;','',[rfReplaceAll, rfIgnoreCase]);
    if p>i then Result:=StringReplace(Result,' Months','',[rfReplaceAll, rfIgnoreCase]);
    if p>i then Result:=StringReplace(Result,'''','',[rfReplaceAll, rfIgnoreCase]);
    if (Length(Result)=1) and (Result[1]='-') then Result:='';
  end;
end;

function ParamExt(Par: AnsiString; Str: AnsiString): Boolean;
begin
  if ParamGet(Par,Str)<>'' then Result:=True else Result:=False;
end;

function GetNYT: String;
begin
  Result:='';
  DateTimeToString(Result, 'yyyy-mm-dd hh:nn:ss', Now());
end;

function CurToBank(Str: AnsiString): AnsiString;
begin
  Result:='UNK';
  if Str='USD' then Result:='FED';
  if Str='EUR' then Result:='ECB';
  if Str='AUD' then Result:='RBA';
  if Str='JPY' then Result:='BOJ';
  if Str='CAD' then Result:='BOC';
  if Str='CHF' then Result:='SNB';
  if Str='NZD' then Result:='RBNZ';
  if Str='GBP' then Result:='BOE';
end;

{TQuery}

constructor TQuery.Create;
begin
  Reset;
end;

destructor TQuery.Destroy;
begin
  Active:=False;
  Reset;
  inherited Destroy;
end;

procedure TQuery.Reset;
begin
  FActive:=False;
  DoReset;
end;

procedure TQuery.SetActive(Act: Boolean);
begin
  if (FActive=False) and (Act=True) and (DoActivate=True) then FActive:=True;
  if (FActive=True) and (Act=False) and (DoDeactivate=True) then FActive:=False;
end;

function TQuery.GetUrlObject(Url: String): ISuperObject;
var l: Integer;
    n: Integer;
    m: Integer;
    q: Integer;
    b: String;
    s: String;
    d: String;
    k: String;
    O: ISuperObject;
    H: ISuperObject;
    P: ISuperObject;
    z: String;
    e: Integer;
begin
  Url:=Trim(Url);
  if Url<>'' then
  begin
    b:=':/\?;#&';
    q:=0;
    s:='';
    try
      d:=HTTPDecode(Url);
    except
      d:=StringReplace(Url,'%','',[rfReplaceAll]);
    end;
    Result:=SO('{"raw":[]}');
    Result.S['org']:=Url;
    Result.S['url']:=d;
    if (Pos(':\',d)>0) or (Pos(':/',d)>0) then
    begin
      O:=SO;
      H:=SA([]);
      P:=SA([]);
      for n:=1 to Length(d) do
      begin
        if pos(d[n],b)=0 then
        begin
          s:=s+d[n];
        end
        else
        begin
          if d[n]=':' then Delete(b,1,1);
          if d[n]='?' then b:='&';
          if s<>'' then
          begin
            if q=0 then
            begin
              Result.A['raw'].Add(LowerCase(Trim(s)));
            end
            else
            begin
              if q=1 then
              begin
                k:='';
                for m:=1 to Length(S) do
                begin
                  if pos(s[m],':@.')=0 then
                  begin
                    k:=k+s[m];
                  end
                  else
                  begin
                    if k<>'' then
                    begin
                      H.AsArray.Add(Trim(k));
                      k:='';
                    end;
                  end;
                end;
                if k<>'' then
                begin
                  H.AsArray.Add(Trim(k));
                  k:='';
                end;
              end
              else
              begin
                l:=pos('=',s);
                if l>0 then
                begin
                  z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
                  z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
                  z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
                  O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
                end
                else
                begin
                  P.AsArray.Add(Trim(s));
                end;
              end;
            end;
            q:=q+1;
            s:='';
          end;
        end;
      end;
      if s<>'' then
      begin
        if q=0 then
        begin
          Result.A['raw'].Add(LowerCase(Trim(s)));
        end
        else
        begin
          if q=1 then
          begin
            k:='';
            for m:=1 to Length(S) do
            begin
              if pos(s[m],':@.')=0 then
              begin
                k:=k+s[m];
              end
              else
              begin
                if k<>'' then
                begin
                  H.AsArray.Add(Trim(k));
                  k:='';
                end;
              end;
            end;
            if k<>'' then
            begin
              H.AsArray.Add(Trim(k));
              k:='';
            end;
          end
          else
          begin
            l:=pos('=',s);
            if l>0 then
            begin
              z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
              z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
              z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
              O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
            end
            else
            begin
              P.AsArray.Add(Trim(s));
            end;
          end;
        end;
        q:=q+1;
        s:='';
      end;
      Result.A['raw'].Add(H);
      Result.A['raw'].Add(P);
      Result.A['raw'].Add(O);
    end
    else
    begin
      if Pos(';',d)>0 then
      begin
        for n:=1 to Length(d) do
        begin
          if d[n]<>';' then
          begin
            s:=s+d[n];
          end
          else
          begin
            if s<>'' then
            begin
              Result.A['raw'].Add(Trim(s));
              s:='';
            end;
          end;
        end;
        if s<>'' then
        begin
          Result.A['raw'].Add(s);
          s:='';
        end;
      end
      else
      begin
        for n:=1 to Length(d) do
        begin
          if d[n]<>' ' then
          begin
            s:=s+d[n];
          end
          else
          begin
            if s<>'' then
            begin
              Result.A['raw'].Add(s);
              s:='';
            end;
          end;
        end;
        if s<>'' then
        begin
          Result.A['raw'].Add(s);
          s:='';
        end;
      end;
    end;
    Result.I['dtm.dt']:=DateTimeToUnix(Now);
    Result.S['dtm.st']:=DateTimeToStr(UnixToDateTime(Result.I['dtm.dt']));
    if Result.A['raw'].Length>0 then
    begin
      Result.S['obj.link.protocol']:=LowerCase(Result.A['raw'].S[0]);

      if (Result.S['obj.link.protocol']='http') or
         (Result.S['obj.link.protocol']='https') or
         (Result.S['obj.link.protocol']='service') or
         (Result.S['obj.link.protocol']='database') then
      begin
        Result.S['obj.link.name']:='localhost';
        Result.S['obj.link.group']:='localhost';
        Result.S['obj.link.domain']:='localhost';
        if Result.S['obj.link.protocol']='http' then Result.I['obj.link.port']:=80 else Result.I['obj.link.port']:=443;
        Result.S['obj.link.path']:='/';
        if Result.A['raw'].Length>1 then
        begin
          if Result.A['raw'].O[1].AsArray.Length>2 then
          begin
            Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
            Result.S['obj.link.group']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-3]);
            Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
            for n:=Result.A['raw'].O[1].AsArray.Length-4 downto 0 do
            begin
              Result.S['obj.link.group']:=Result.S['obj.link.group']+'.'+LowerCase(Result.A['raw'].O[1].AsArray.S[n]);
            end;
            Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
          end
          else
          begin
            if Result.A['raw'].O[1].AsArray.Length=2 then
            begin
              Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
              Result.S['obj.link.group']:='main';
              Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
              Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
            end
            else
            begin
              if Result.A['raw'].O[1].AsArray.Length=1 then
              begin
                Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[0]);
                Result.S['obj.link.group']:=Result.S['obj.link.name'];
                Result.S['obj.link.domain']:=Result.S['obj.link.name'];
                Result.S['obj.link.top']:=Result.S['obj.link.name'];
              end;
            end;
          end;
          if Result.A['raw'].Length>2 then
          begin
            if Result.A['raw'].O[2].AsArray.Length>0 then
            begin
              Result.S['obj.link.path']:='';
              for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
              begin
                Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
              end;
            end;
          end;


          if Result.A['raw'].Length=4 then
          begin
            Result.O['obj.link.value']:=Result.A['raw'].O[3];
            if Result.S['obj.option.typ']='' then
            begin
              if (Result.A['raw'].O[2].AsArray.Length>0) and
                 (pos('.',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])>0) and
                 (pos(',',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])=0) then
              begin
                Result.S['obj.option.typ']:='file';
                Result.S['obj.option.url']:=Result.S['org'];
                Result.S['obj.option.dsc']:=Result.S['dsc'];
                Result.S['obj.option.res']:=Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1];
                Result.S['obj.option.nam']:=Result.S['obj.option.res'];
                Result.S['obj.option.ext']:=ExtractFileExt(Result.S['obj.option.res']);
              end
              else
              begin
                Result.S['obj.option.typ']:='link';
                Result.S['obj.option.url']:=Result.S['org'];
                Result.S['obj.option.dsc']:=Result.S['dsc'];
              end;
            end;
          end;
        end;
      end
      else
      begin
        if Result.S['obj.link.protocol']='ftp' then
        begin
          Result.S['obj.link.name']:='localhost';
          Result.S['obj.link.group']:='localhost';
          Result.S['obj.link.domain']:='localhost';
          Result.I['obj.link.port']:=21;
          Result.S['obj.link.login']:='anonymous';
          Result.S['obj.link.password']:='anonymous';
          Result.S['obj.link.path']:='/';
          if Result.A['raw'].Length>1 then
          begin
            if Result.A['raw'].O[1].AsArray.Length=4 then
            begin

            end;
          end;
          //ftp://user:password@host:port/path
          if Result.A['raw'].Length>2 then
          begin
            for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
            begin
              Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
            end;
          end;
        end
        else
        begin
          if Result.S['obj.link.protocol']='file' then
          begin
            if (Result.A['raw'].Length>3) and
               (Result.A['raw'].O[1].AsArray.Length=1) then
            begin
              Result.S['obj.link.value.full']:=Result.A['raw'].O[1].AsArray.S[0]+':';
              for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
              begin
                Result.S['obj.link.value.full']:=Result.S['obj.link.value.full']+'\'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
              end;
              Result.B['obj.link.value.exst']:=FileExists(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.path']:=ExtractFilePath(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.name']:=ExtractFileName(Result.S['obj.link.value.full']);
              Result.S['obj.link.value.extn']:=ExtractFileExt(Result.S['obj.link.value.full']);
            end;
          end;
        end;
      end;
    end;
  end
  else
  begin
    Result:=nil;
  end;
end;

function TQuery.Get(Url: AnsiString): ISuperObject;
var Obj: ISuperObject;
begin
  if FActive=True then
  begin
    Obj:=GetUrlObject(Url);
    if Assigned(Obj) then
    begin
      Result:=DoGet(Obj);
      if not Assigned(Result) then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=-1001;
        Result.S['description']:='Result is null';
      end;
    end
    else
    begin
      Result:=SO;
      Result.B['status']:=False;
      Result.I['code']:=-1000;
      Result.S['description']:='Url is null';
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=-2;
    Result.S['description']:='Query is not activated';
  end;
end;

function TQuery.Post(Url: AnsiString; Body: ISuperObject): ISuperObject;
var Obj: ISuperObject;
begin
  if FActive=True then
  begin
    Obj:=GetUrlObject(Url);
    if Assigned(Obj)=True then
    begin
      if Assigned(Body) then Result:=DoPost(Obj,Body) else Result:=DoPost(Obj,SO);
      if not Assigned(Result) then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=-3;
        Result.S['description']:='Result is null';
      end;
    end
    else
    begin
      Result:=SO;
      Result.B['status']:=False;
      Result.I['code']:=-1;
      Result.S['description']:='Url is null';
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=-2;
    Result.S['description']:='Query is not activated';
  end;
end;

function TQuery.GetHash(V: AnsiString): AnsiString;
var n: Integer;
    S: Word;
begin
  S:=0;
  if V<>'' then
  begin
    Result:=StringReplace(V,'-','',[rfReplaceAll]);
    Result:=StringReplace(Result,':','',[rfReplaceAll]);
    Result:=StringReplace(Result,' ','',[rfReplaceAll]);
  end
  else
  begin
    Result:='N'+IntToHex(S,4)+'L';
  end;
end;

{TScrapInvestment}

procedure TScrapInvestment.DoReset;
begin
  FStrings:=nil;
  FStringsEx:=nil;
end;

function TScrapInvestment.DoActivate: Boolean;
begin
  FStrings:=TStringList.Create;
  FStrings.LineBreak:=#$0A;
  FStringsEx:=TStringList.Create;
  FStringsEx.LineBreak:=#$0A;
  Result:=True;
end;

function TScrapInvestment.DoDeactivate: Boolean;
begin
  FStrings.Destroy;
  FStringsEx.Destroy;
  Result:=True;
end;

//ReadDataByte;
function TScrapInvestment.CopyArray(dest: TSuperArray; source: TSuperArray): Boolean;
var n: Integer;
begin
  Result:=False;
  if (Assigned(dest)) and (Assigned(source)) then
  begin
    if source.Length>0 then
    begin
      for n:=0 to source.Length-1 do
      begin
        dest.Add(source.O[n]);
      end;
      Result:=True;
    end;
  end;
end;

function TScrapInvestment.ParsingEconomicCalendar(Url: AnsiString; DateFrom: AnsiString; DateTo: AnsiString; HAvoid: Boolean; Importance: Integer): ISuperObject;
var Str: AnsiString;
     n: UInt64;
     a: Integer;
     Obj: ISuperObject;
     Uri: AnsiString;
     H: String;
begin
  Result:=nil;
  FStrings.Text:=StringReplace(HttpPost(Url,DateFrom,DateTo,Importance),'trid','tr id',[rfReplaceAll, rfIgnoreCase]);
  if FStrings.Count>1 then
  begin
    Result:=SO;
    Result.B['status']:=True;
    Result.I['code']:=0;
    Result.S['description']:='Success';
    Result.O['response.widgeteconomiccalendar']:=SO('[]');

    for n:=0 to FStrings.Count-1 do
    begin
      if ParamGet('class',FStrings.Strings[n])='theDay' then
      begin
        H:=UnixToStr(StrToInt(Trim(StringReplace(ParamGet('id',FStrings.Strings[n]),'theDay','',[rfReplaceAll]))));
      end;
      if (pos('eventRowId',FStrings.Strings[n])>0) and
         (ParamExt('onclick',FStrings.Strings[n])=False) and
         (Length(FStrings.Strings[n])<32) then
      begin
        Obj:=SO;
        Obj.S['timestamp']:=H;
      end;
      if Assigned(Obj) then
      begin
        if ParamGet('class',FStrings.Strings[n])='first left time' then Obj.S['time']:=ParamGet('evtStrtTime',FStrings.Strings[n]);
        if pos('flagCur',ParamGet('class',FStrings.Strings[n]))>0 then Obj.S['country']:=ParamGet('title',FStrings.Strings[n]);
        if pos('flagCur',ParamGet('class',FStrings.Strings[n]))>0 then Obj.S['currency']:=ParamDefA(FStrings.Strings[n]);
        Obj.S['desc']:='Economic calendar';
        //if ParamGet('class',FStrings.Strings[n])='sentiment' then Obj.S['desc']:=ParamGet('title',FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='left event' then Obj.S['title']:=ParamDef(FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='left textNum sentiment' then
        begin
          if pos('Holiday',FStrings.Strings[n])>0 then Obj.B['holiday']:=True else Obj.B['holiday']:=False;
        end;
        if Copy(ParamGet('class',FStrings.Strings[n]),1,4)='prev' then Obj.S['prev']:=ParamDef(FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='fore' then Obj.S['fore']:=ParamDef(FStrings.Strings[n]);
        if Copy(ParamGet('class',FStrings.Strings[n]),1,8)='bold act' then Obj.S['act']:=ParamDef(FStrings.Strings[n]);
      end;
      if Assigned(Obj) then
      begin
        if ((ParamGet('colspan',FStrings.Strings[n])='5') and (ParamGet('class',FStrings.Strings[n])='left event')) then
        begin
          if Obj.S['event']='' then Obj.S['event']:='ECC';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['country']='' then Obj.S['country']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['prev']='' then Obj.S['prev']:='NULL';
          if Obj.S['fore']='' then Obj.S['fore']:='NULL';
          if Obj.S['act']='' then Obj.S['act']:='NULL';
          if Obj.S['holiday']='' then Obj.B['holiday']:=False;
          if Obj.S['importance']='' then Obj.S['importance']:=IntToStr(Importance);
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          if Obj.B['holiday']=True then Obj.S['title']:=Obj.S['title']+' - HOLIDAY';
          if HAvoid=True then
          begin
            if Obj.B['holiday']=False then Result.A['response.widgeteconomiccalendar'].Add(Obj);
          end
          else
          begin
            Result.A['response.widgeteconomiccalendar'].Add(Obj);
          end;
          Obj:=nil;
        end;
      end;
    end;

    for n:=0 to FStrings.Count-1 do
    begin
      if (ParamExt('onclick',FStrings.Strings[n])=True) and (ParamExt('id',FStrings.Strings[n])=True) then
      begin
        Obj:=SO;
        Obj.S['timestamp']:=ParamGet('event_timestamp',FStrings.Strings[n]);
      end;
      if Assigned(Obj) then
      begin
        if ParamGet('class',FStrings.Strings[n])='first left time' then Obj.S['time']:=ParamGet('evtStrtTime',FStrings.Strings[n]);
        if pos('flagCur',ParamGet('class',FStrings.Strings[n]))>0 then Obj.S['country']:=ParamGet('title',FStrings.Strings[n]);
        if pos('flagCur',ParamGet('class',FStrings.Strings[n]))>0 then Obj.S['currency']:=ParamDefA(FStrings.Strings[n]);
        Obj.S['desc']:='Economic calendar';
        //if ParamGet('class',FStrings.Strings[n])='sentiment' then Obj.S['desc']:=ParamGet('title',FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='left event' then Obj.S['title']:=ParamDef(FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='left textNum sentiment' then
        begin
          if pos('Holiday',FStrings.Strings[n])>0 then Obj.B['holiday']:=True else Obj.B['holiday']:=False;
        end;
        if Copy(ParamGet('class',FStrings.Strings[n]),1,4)='prev' then Obj.S['prev']:=ParamDef(FStrings.Strings[n]);
        if ParamGet('class',FStrings.Strings[n])='fore' then Obj.S['fore']:=ParamDef(FStrings.Strings[n]);
        if Copy(ParamGet('class',FStrings.Strings[n]),1,8)='bold act' then Obj.S['act']:=ParamDef(FStrings.Strings[n]);
      end;
      if Assigned(Obj) then
      begin
        if (ParamGet('class',FStrings.Strings[n])='displayNone') then
        begin
          if Obj.S['event']='' then Obj.S['event']:='ECC';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['country']='' then Obj.S['country']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['prev']='' then Obj.S['prev']:='NULL';
          if Obj.S['fore']='' then Obj.S['fore']:='NULL';
          if Obj.S['act']='' then Obj.S['act']:='NULL';
          if Obj.S['holiday']='' then Obj.B['holiday']:=False;
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          if Obj.B['holiday']=True then Obj.S['title']:=Obj.S['title']+' - HOLIDAY';
          if Obj.S['importance']='' then Obj.S['importance']:=IntToStr(Importance);
          if HAvoid=True then
          begin
            if Obj.B['holiday']=False then Result.A['response.widgeteconomiccalendar'].Add(Obj);
          end
          else
          begin
            Result.A['response.widgeteconomiccalendar'].Add(Obj);
          end;
          Obj:=nil;
        end;
      end;
    end;

  end
  else
  begin
    if FStrings.Count=1 then
    begin
      Result:=SO;
      Result.B['status']:=False;
      Result.I['code']:=-4;
      Result.S['description']:=FStrings.Strings[0];
    end;
  end;
end;

function TScrapInvestment.ParsingCentralBanksRateEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;
const

  DEEP_HOLE = 8;

var lt: UInt64;
    Obj: ISuperObject;
    n: Integer;
    T1,T2: AnsiString;
    q: Integer;
    cur: AnsiString;
begin
  Result:=ParsingCentralBanksRate(Url1,Cur,Lt);
  if Assigned(Result)=True then
  begin
    for q:=0 to DEEP_HOLE do
    begin
      FStrings.Text:=StringReplace(HttpPostB(Url2,Id,lt),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
      if FStrings.Count>1 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if pos('ecTitle float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
          begin
            T1:=ParamDef(FStrings.Strings[n]);
            Break;
          end;
        end;
        for n:=0 to FStrings.Count-1 do
        begin
          if pos('Currency:',FStrings.Strings[n])>0 then
          begin
            T2:=ParamDef(FStrings.Strings[n+1]);
            Break;
          end;
        end;
        for n:=0 to FStrings.Count-1 do
        begin
          if pos('historicEvent_',FStrings.Strings[n])>0 then
          begin
            lt:=StrToUnix(ParamGet('event_timestamp',FStrings.Strings[n+0]));
            Obj:=SO;
            Obj.S['timestamp']:=ParamGet('event_timestamp',FStrings.Strings[n+0]);
            Obj.S['time']:=ParamDef(FStrings.Strings[n+2]);
            Obj.S['currency']:=Cur;
            Obj.S['desc']:='Interest Rate Decision';
            Obj.S['title']:=T1;
            Obj.S['period']:='0';
            Obj.S['event']:='IRD';
            if ParamDefE('on">',FStrings.Strings[n+3])<>'' then
            begin
              Obj.S['actual']:=ParamDefE('on">',FStrings.Strings[n+3]);
            end
            else
            begin
              Obj.S['actual']:=ParamDefE('ed">',FStrings.Strings[n+3]);
            end;
            Obj.S['forecast']:=ParamDef(FStrings.Strings[n+4]);
            Obj.S['previous']:=ParamDef(FStrings.Strings[n+5]);
            if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
            if Obj.S['time']='' then Obj.S['time']:='00:00';
            if Obj.S['currency']='' then Obj.S['currency']:='NULL';
            if Obj.S['desc']='' then Obj.S['desc']:='NULL';
            if Obj.S['title']='' then Obj.S['title']:='NULL';
            if Obj.S['period']='' then Obj.S['period']:='NULL';
            if Obj.S['event']='' then Obj.S['event']:='IRD';
            if Obj.S['actual']='' then Obj.S['actual']:='NULL';
            if Obj.S['forecast']='' then Obj.S['forecast']:='NULL';
            if Obj.S['previous']='' then Obj.S['previous']:='NULL';
            if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
            Result.A['response.widgetcentralbankrate'].Add(Obj);
          end;
        end;
      end;
    end;
  end;
end;

function TScrapInvestment.ParsingCentralBanksRate(Url: AnsiString; var Cur: AnsiString; var Lt: UInt64): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
begin
  Result:=nil;
  lt:=0;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=200;
      Result.S['description']:='Success';
      Result.O['response.widgetcentralbankrate']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('ecTitle float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Currency:',FStrings.Strings[n])>0 then
        begin
          T2:=ParamDef(FStrings.Strings[n+1]);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('historicEvent_',FStrings.Strings[n])>0 then
        begin
          lt:=StrToUnix(ParamGet('event_timestamp',FStrings.Strings[n+0]));
          cur:=CurToBank(T2);
          Obj:=SO;
          Obj.S['timestamp']:=ParamGet('event_timestamp',FStrings.Strings[n+0]);
          Obj.S['time']:=ParamDef(FStrings.Strings[n+2]);
          Obj.S['currency']:=cur;
          Obj.S['desc']:='Interest Rate Decision';
          Obj.S['title']:=T1;
          Obj.S['period']:='0';
          Obj.S['event']:='IRD';
          if ParamDefE('on">',FStrings.Strings[n+3])<>'' then
          begin
            Obj.S['actual']:=ParamDefE('on">',FStrings.Strings[n+3]);
          end
          else
          begin
            Obj.S['actual']:=ParamDefE('ed">',FStrings.Strings[n+3]);
          end;
          Obj.S['forecast']:=ParamDef(FStrings.Strings[n+4]);
          Obj.S['previous']:=ParamDef(FStrings.Strings[n+5]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='00:00';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['period']='' then Obj.S['period']:='NULL';
          if Obj.S['event']='' then Obj.S['event']:='IRD';
          if Obj.S['actual']='' then Obj.S['actual']:='NULL';
          if Obj.S['forecast']='' then Obj.S['forecast']:='NULL';
          if Obj.S['previous']='' then Obj.S['previous']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetcentralbankrate'].Add(Obj);
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=-4;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingFinancialSummaryAnnual(Url: AnsiString): AnsiString;
var n: Integer;
begin
  Result:='';
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStringsEx.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStringsEx.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    for n:=0 to FStringsEx.Count-1 do
    begin
      if pos('Operating Cash Flow',FStringsEx.Strings[n])>0 then
      begin
        Result:=ParamDefE('dirLtr bold">',FStringsEx.Strings[n]);
        Break;
      end;
    end;
  end;
end;

function TScrapInvestment.ParsingFinancialSummary(Url1: AnsiString; Url2: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
  Tmp: ISuperObject;
begin
  Result:=nil;
  if Url1<>'' then
  begin
    if Pos('://',Url1)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url1),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url1),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetfinancialsummary']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray first toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      Tmp:=SO['[]'];
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Period Ending:',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+1])+'/'+ParamDefE('_11">',FStrings.Strings[n+1])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+2])+'/'+ParamDefE('_11">',FStrings.Strings[n+2])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+3])+'/'+ParamDefE('_11">',FStrings.Strings[n+3])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+4])+'/'+ParamDefE('_11">',FStrings.Strings[n+4])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Break;
        end;
      end;
      Obj:=SO;
      Obj.S['timestamp']:=GetNYT;
      Obj.S['currency']:=T2;
      Obj.S['period']:=P;
      Obj.S['desc']:='Financial summary';
      Obj.S['title']:=T1;
      for n:=0 to FStrings.Count-1 do
      begin
        if ParamGet('class',FStrings.Strings[n])='flagCur' then Obj.S['currency']:=ParamDefA(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='arial_12') and (ParamGet('id',FStrings.Strings[n])='profile-story') then Obj.S['text']:=ParamDef(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Gross margin',FStrings.Strings[n])>0) then Obj.S['grossmargin']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Operating margin',FStrings.Strings[n])>0) then Obj.S['operatingmargin']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Net Profit margin',FStrings.Strings[n])>0) then Obj.S['netprofitmargin']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Return on Investment',FStrings.Strings[n])>0) then Obj.S['returnoninvestment']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Quick Ratio',FStrings.Strings[n])>0) then Obj.S['quickratio']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Current Ratio',FStrings.Strings[n])>0) then Obj.S['currentratio']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('LT Debt to Equity',FStrings.Strings[n])>0) then Obj.S['ltdebttoequity']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Total Debt to Equity',FStrings.Strings[n])>0) then Obj.S['totaldebttoequity']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Cash Flow/Share',FStrings.Strings[n])>0) then Obj.S['cashflowshare']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Revenue/Share',FStrings.Strings[n])>0) then Obj.S['revenueshare']:=ParamDefB(FStrings.Strings[n]);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Operating Cash Flow',FStrings.Strings[n])>0) then Obj.S['operatingcashflow1']:=ParsingFinancialSummaryAnnual(Url2);
        if (ParamGet('class',FStrings.Strings[n])='infoLine') and (pos('Operating Cash Flow',FStrings.Strings[n])>0) then Obj.S['operatingcashflow2']:=ParamDefB(FStrings.Strings[n]);
      end;
      if Assigned(Obj) then
      begin
        if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
        if Obj.S['currency']='' then Obj.S['currency']:='NULL';
        if Obj.S['desc']='' then Obj.S['desc']:='NULL';
        if Obj.S['title']='' then Obj.S['title']:='NULL';
        if Obj.S['text']='' then Obj.S['text']:='NULL';
        if Obj.S['grossmargin']='' then Obj.S['grossmargin']:='NULL';
        if Obj.S['operatingmargin']='' then Obj.S['operatingmargin']:='NULL';
        if Obj.S['netprofitmargin']='' then Obj.S['netprofitmargin']:='NULL';
        if Obj.S['returnoninvestment']='' then Obj.S['returnoninvestment']:='NULL';
        if Obj.S['quickratio']='' then Obj.S['quickratio']:='NULL';
        if Obj.S['currentratio']='' then Obj.S['currentratio']:='NULL';
        if Obj.S['ltdebttoequity']='' then Obj.S['ltdebttoequity']:='NULL';
        if Obj.S['totaldebttoequity']='' then Obj.S['totaldebttoequity']:='NULL';
        if Obj.S['cashflowshare']='' then Obj.S['cashflowshare']:='NULL';
        if Obj.S['revenueshare']='' then Obj.S['revenueshare']:='NULL';
        if Obj.S['operatingcashflow1']='' then Obj.S['operatingcashflow']:='NULL';
        if Obj.S['operatingcashflow2']='' then Obj.S['operatingcashflow']:='NULL';
        if Obj.S['event']='' then
        begin
          if Pos('annual',Url1)>0 then Obj.S['event']:='ASM' else Obj.S['event']:='FSM';
        end;
        if Obj.S['time']='' then Obj.S['time']:='00:00';
        if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
        Result.A['response.widgetfinancialsummary'].Add(Obj);
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=-4;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingSummaryIncomeStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
  P: AnsiString;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=200;
      Result.S['description']:='Success';
      Result.O['response.widgetsummaryincomestatement']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      i:=0;
      for n:=0 to FStrings.Count-1 do
      begin
        if (i>=1) and (pos('<th>',FStrings.Strings[n])>0) then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n])+'/'+ParamDefE('11">',FStrings.Strings[n])+' 00:00:00');
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Summary Income statement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Obj.S['event']='' then
          begin
            if (Pos('annual',Url)>0) or (Pos('ajax',Url)>0) then Obj.S['event']:='AST' else Obj.S['event']:='IST';
          end;
          Result.A['response.widgetsummaryincomestatement'].Add(Obj);
        end
        else
        begin
          i:=0;
        end;
        if (i=0) and (pos('Period Ending:',FStrings.Strings[n])>0) then i:=1;
      end;
      i:=0;
      if Result.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummaryincomestatement'].O[i-1].S['totalrevenue']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('dropDownArrowLightGray',FStrings.Strings[n])>0) and (pos('Revenue',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummaryincomestatement'].O[i-1].S['grossprofit']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Gross Profit',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummaryincomestatement'].O[i-1].S['operatingincome']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Operating Income',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummaryincomestatement'].O[i-1].S['netincome']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Net Income',FStrings.Strings[n])>0) and (pos('Before',FStrings.Strings[n])=0) and (pos('After',FStrings.Strings[n])=0) then i:=1;
        end;
      end;
      if Result.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to Result.A['response.widgetsummaryincomestatement'].Length-1 do
        begin
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['timestamp']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['timestamp']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['time']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['time']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['currency']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['currency']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['period']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['period']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['totalrevenue']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['totalrevenue']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['grossprofit']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['grossprofit']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['operatingincome']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['operatingincome']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['netincome']='' then Result.A['response.widgetsummaryincomestatement'].O[n].S['netincome']:='NULL';
          if Result.A['response.widgetsummaryincomestatement'].O[n].S['hash']='' then
          begin
            Result.A['response.widgetsummaryincomestatement'].O[n].S['hash']:=GetHash(Result.A['response.widgetsummaryincomestatement'].O[n].S['timestamp']+Result.A['response.widgetsummaryincomestatement'].O[n].S['event']+Result.A['response.widgetsummaryincomestatement'].O[n].S['currency']);
          end;
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingSummaryBalanceSheet(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetsummarybalancesheet']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      i:=0;
      for n:=0 to FStrings.Count-1 do
      begin
        if (i>=1) and (pos('<th>',FStrings.Strings[n])>0) then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n])+'/'+ParamDefE('11">',FStrings.Strings[n])+' 00:00:00');
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='SummaryBalanceSheet';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Pos('annual',Url)>0 then Obj.S['event']:='ASH' else Obj.S['event']:='BSH';
          Result.A['response.widgetsummarybalancesheet'].Add(Obj);
        end
        else
        begin
          i:=0;
        end;
        if (i=0) and (pos('Period Ending:',FStrings.Strings[n])>0) then i:=1;
      end;
      i:=0;
      if Result.A['response.widgetsummarybalancesheet'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummarybalancesheet'].O[i-1].S['totalassets']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Total Assets',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetsummarybalancesheet'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummarybalancesheet'].O[i-1].S['totalliabilities']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Total Liabilities',FStrings.Strings[n])>0) and (pos('dropDownArrowLightGray',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetsummarybalancesheet'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetsummarybalancesheet'].O[i-1].S['totalequity']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Total Equity',FStrings.Strings[n])>0) and (pos('dropDownArrowLightGray',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      if Result.A['response.widgetsummarybalancesheet'].Length>0 then
      begin
        for n:=0 to Result.A['response.widgetsummarybalancesheet'].Length-1 do
        begin
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['timestamp']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['timestamp']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['time']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['time']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['currency']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['currency']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['period']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['period']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['totalassets']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['totalassets']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['totalliabilities']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['totalliabilities']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['totalequity']='' then Result.A['response.widgetsummarybalancesheet'].O[n].S['totalequity']:='NULL';
          if Result.A['response.widgetsummarybalancesheet'].O[n].S['hash']='' then
          begin
            Result.A['response.widgetsummarybalancesheet'].O[n].S['hash']:=GetHash(Result.A['response.widgetsummarybalancesheet'].O[n].S['timestamp']+Result.A['response.widgetsummarybalancesheet'].O[n].S['event']+Result.A['response.widgetsummarybalancesheet'].O[n].S['currency']);
          end;
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingIncomeStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
  Tmp: ISuperObject;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetincomestatement']:=SO('[]');
      Tmp:=SO['[]'];
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Period Ending:',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+1])+'/'+ParamDefE('_11">',FStrings.Strings[n+1])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+2])+'/'+ParamDefE('_11">',FStrings.Strings[n+2])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+3])+'/'+ParamDefE('_11">',FStrings.Strings[n+3])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+4])+'/'+ParamDefE('_11">',FStrings.Strings[n+4])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('<td><span class="',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='incomestatement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Pos('annual',Url)>0 then Obj.S['event']:='AIS' else Obj.S['event']:='PIS';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+1]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetincomestatement'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='incomestatement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Pos('annual',Url)>0 then Obj.S['event']:='ZIS' else Obj.S['event']:='QIS';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+2]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetincomestatement'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='incomestatement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Pos('annual',Url)>0 then Obj.S['event']:='YIS' else Obj.S['event']:='RIS';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+3]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetincomestatement'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='incomestatement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          if Pos('annual',Url)>0 then Obj.S['event']:='WIS' else Obj.S['event']:='SIS';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+4]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetincomestatement'].Add(Obj);
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingBalanceSheet(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
  Tmp: ISuperObject;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetbalancesheet']:=SO('[]');
      Tmp:=SO['[]'];
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Period Ending:',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+1])+'/'+ParamDefE('_11">',FStrings.Strings[n+1])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+2])+'/'+ParamDefE('_11">',FStrings.Strings[n+2])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+3])+'/'+ParamDefE('_11">',FStrings.Strings[n+3])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+4])+'/'+ParamDefE('_11">',FStrings.Strings[n+4])+' 00:00:00');
          Tmp.AsArray.Add(Obj);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('<td><span class="',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='balancesheet';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='BSH';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+1]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetbalancesheet'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='balancesheet';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='BSH';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+2]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetbalancesheet'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='balancesheet';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='BSH';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+3]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetbalancesheet'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='balancesheet';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='BSH';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+4]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetbalancesheet'].Add(Obj);
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingSummaryCashFlowStatement(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetcashstatement']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      i:=0;
      for n:=0 to FStrings.Count-1 do
      begin
        if (i>=1) and (pos('<th>',FStrings.Strings[n])>0) then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n])+'/'+ParamDefE('11">',FStrings.Strings[n])+' 00:00:00');
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Cash flow statement';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='CFS';
         Result.A['response.widgetcashstatement'].Add(Obj);
        end
        else
        begin
          i:=0;
        end;
        if (i=0) and (pos('Period Ending:',FStrings.Strings[n])>0) then i:=1;
      end;
      i:=0;
      if Result.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetcashstatement'].O[i-1].S['cashfromoperating']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Cash From Operating',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetcashstatement'].O[i-1].S['cashfrominvesting']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Cash From Investing',FStrings.Strings[n])>0) and (pos('dropDownArrowLightGray',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetcashstatement'].O[i-1].S['cashfromfinancing']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Cash From Financing',FStrings.Strings[n])>0) and (pos('dropDownArrowLightGray',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      i:=0;
      if Result.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('<td>',FStrings.Strings[n])>0) then
          begin
            Result.A['response.widgetcashstatement'].O[i-1].S['cashnetchangein']:=ParamDefE('<td>',FStrings.Strings[n]);
            i:=i+1;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('Net Change in',FStrings.Strings[n])>0) and (pos('bold',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
      if Result.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to Result.A['response.widgetcashstatement'].Length-1 do
        begin
          if Result.A['response.widgetcashstatement'].O[n].S['timestamp']='' then Result.A['response.widgetcashstatement'].O[n].S['timestamp']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['time']='' then Result.A['response.widgetcashstatement'].O[n].S['time']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['currency']='' then Result.A['response.widgetcashstatement'].O[n].S['currency']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['period']='' then Result.A['response.widgetcashstatement'].O[n].S['period']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['cashfromoperating']='' then Result.A['response.widgetcashstatement'].O[n].S['cashfromoperating']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['cashfrominvesting']='' then Result.A['response.widgetcashstatement'].O[n].S['cashfrominvesting']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['cashfromfinancing']='' then Result.A['response.widgetcashstatement'].O[n].S['cashfromfinancing']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['cashnetchangein']='' then Result.A['response.widgetcashstatement'].O[n].S['cashnetchangein']:='NULL';
          if Result.A['response.widgetcashstatement'].O[n].S['hash']='' then
          begin
            Result.A['response.widgetcashstatement'].O[n].S['hash']:=GetHash(Result.A['response.widgetcashstatement'].O[n].S['timestamp']+Result.A['response.widgetcashstatement'].O[n].S['event']+Result.A['response.widgetcashstatement'].O[n].S['currency']);
          end;
        end;
      end;

    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingDividendsEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;
const

 DEEP_HOLE = 4;

var n: Integer;
    T1,T2: AnsiString;
    i: Integer;
    Obj: ISuperObject;
    lt: UInt64;
    q: Integer;
begin
  Result:=ParsingDividends(Url1,lt);
  if Assigned(Result)=True then
  begin
    for q:=0 to DEEP_HOLE do
    begin
      FStrings.Text:=StringReplace(HttpPostQ('https://www.investing.com/equities/MoreDividendsHistory',Id,lt),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
      if FStrings.Count>1 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
          begin
            T1:=ParamDef(FStrings.Strings[n]);
            T2:=ParamSkb(FStrings.Strings[n]);
            Break;
          end;
        end;
        i:=0;
        for n:=0 to FStrings.Count-1 do
        begin
          if (i>=1) and (pos('data-valu',FStrings.Strings[n])>0) then
          begin
            lt:=StrToInt(ParamGet('data-value',FStrings.Strings[n+0]));
            Obj:=SO;
            Obj.S['timestamp']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+0]));
            Obj.S['unixtime']:=ParamGet('data-value',FStrings.Strings[n+0]);
            Obj.S['time']:='00:00';
            if T2='' then
            begin
              case id of
                277: Obj.S['currency']:='MMM';
                8182: Obj.S['currency']:='AXP';
                6408: Obj.S['currency']:='AAPL';
                271: Obj.S['currency']:='KO';
              else
                Obj.S['currency']:='NULL';
              end;
            end
            else
            begin
              Obj.S['currency']:=T2;
            end;
            Obj.S['desc']:='Dividents';
            Obj.S['title']:=T1;
            Obj.S['period']:='1';
            Obj.S['event']:='DVD';
            Obj.S['dividend']:=ParamDef(FStrings.Strings[n+1]);
            Obj.S['type']:=ParamGet('title',FStrings.Strings[n+3]);
            if Obj.S['type']='Monthly' then Obj.S['mark']:='1M';
            if Obj.S['type']='Quarterly' then Obj.S['mark']:='3M';
            if Obj.S['type']='Semi-Annual' then Obj.S['mark']:='6M';
            if Obj.S['type']='Annual' then Obj.S['mark']:='12M';
            if Obj.S['type']='Trailing Twelve Months' then Obj.S['mark']:='TTM';
            if Obj.S['type']='Other' then Obj.S['mark']:='O';
            if Obj.S['type']='Interim' then Obj.S['mark']:='I';
            if Obj.S['type']='Final' then Obj.S['mark']:='F';
            if Obj.S['type']='Bonus' then Obj.S['mark']:='B';
            Obj.S['exdividentdate']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+0]));
            Obj.S['paymentdate']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+5]));
            Obj.S['yield']:=ParamDef(FStrings.Strings[n+6]);
            if Obj.S['mark']='' then Obj.S['mark']:='NULL';
            if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
            if Obj.S['time']='' then Obj.S['time']:='00:00';
            if Obj.S['currency']='' then Obj.S['currency']:='NULL';
            if Obj.S['desc']='' then Obj.S['desc']:='NULL';
            if Obj.S['title']='' then Obj.S['title']:='NULL';
            if Obj.S['period']='' then Obj.S['period']:='NULL';
            if Obj.S['dividend']='' then Obj.S['dividend']:='NULL';
            if Obj.S['type']='' then Obj.S['type']:='NULL';
            if Obj.S['exdividentdate']='' then Obj.S['exdividentdate']:='NULL';
            if Obj.S['paymentdate']='' then Obj.S['paymentdate']:='NULL';
            if Obj.S['yield']='' then Obj.S['yield']:='NULL';
            if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
            Result.A['response.widgetdividends'].Add(Obj);
            i:=0;
          end
          else
          begin
            i:=0;
          end;
          if (i=0) and (pos('<tr',FStrings.Strings[n])>0) and (pos('event_timestamp',FStrings.Strings[n])>0) then i:=1;
        end;
      end;
    end;
  end;
end;

function TScrapInvestment.ParsingDividends(Url: AnsiString; var Lt: UInt64): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
begin
  Result:=nil;
  lt:=0;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetdividends']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      i:=0;
      for n:=0 to FStrings.Count-1 do
      begin
        if (i>=1) and (pos('data-valu',FStrings.Strings[n])>0) then
        begin
          lt:=StrToInt(ParamGet('data-value',FStrings.Strings[n+0]));
          Obj:=SO;
          Obj.S['timestamp']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+0]));
          Obj.S['unixtime']:=ParamGet('data-value',FStrings.Strings[n+0]);
          Obj.S['time']:='00:00';
          Obj.S['currency']:=T2;
          Obj.S['desc']:='Dividents';
          Obj.S['title']:=T1;
          Obj.S['period']:='1';
          Obj.S['event']:='DVD';
          Obj.S['dividend']:=ParamDef(FStrings.Strings[n+1]);
          Obj.S['type']:=ParamGet('title',FStrings.Strings[n+3]);
          if Obj.S['type']='Monthly' then Obj.S['mark']:='1M';
          if Obj.S['type']='Quarterly' then Obj.S['mark']:='3M';
          if Obj.S['type']='Semi-Annual' then Obj.S['mark']:='6M';
          if Obj.S['type']='Annual' then Obj.S['mark']:='12M';
          if Obj.S['type']='Trailing Twelve Months' then Obj.S['mark']:='TTM';
          if Obj.S['type']='Other' then Obj.S['mark']:='O';
          if Obj.S['type']='Interim' then Obj.S['mark']:='I';
          if Obj.S['type']='Final' then Obj.S['mark']:='F';
          if Obj.S['type']='Bonus' then Obj.S['mark']:='B';
          Obj.S['exdividentdate']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+0]));
          Obj.S['paymentdate']:=FormatMySqlUnix(ParamGet('data-value',FStrings.Strings[n+5]));
          Obj.S['yield']:=ParamDef(FStrings.Strings[n+6]);
          if Obj.S['mark']='' then Obj.S['mark']:='NULL';
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='00:00';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['period']='' then Obj.S['period']:='NULL';
          if Obj.S['dividend']='' then Obj.S['dividend']:='NULL';
          if Obj.S['type']='' then Obj.S['type']:='NULL';
          if Obj.S['exdividentdate']='' then Obj.S['exdividentdate']:='NULL';
          if Obj.S['paymentdate']='' then Obj.S['paymentdate']:='NULL';
          if Obj.S['yield']='' then Obj.S['yield']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetdividends'].Add(Obj);
          i:=0;
        end
        else
        begin
          i:=0;
        end;
        if (i=0) and (pos('<tr',FStrings.Strings[n])>0) and (pos('event_timestamp',FStrings.Strings[n])>0) then i:=1;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingLatestRelease(Url: AnsiString): AnsiString;
var S: AnsiString;
    n: UInt64;
begin
  Result:=FormatMySqlNow;
  if Url<>'' then
  begin
    FStrings.Text:=StringReplace(httpget(url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    if FStrings.Count>1 then
    begin
      S:='';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Latest Release',FStrings.Strings[n])>0 then
        begin
          S:=AnsiUpperCase(ParamDefE('ld">',FStrings.Strings[n]));
          if S<>'' then
          begin
            S:=StringReplace(S,'JAN','01',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'FEB','02',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'MAR','03',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'APR','04',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'MAY','05',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'JUN','06',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'JUL','07',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'AUG','08',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'SEP','09',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'OCT','10',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'NOV','11',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,'DEC','12',[rfReplaceAll, rfIgnoreCase]);
            S:=StringReplace(S,', ',' ',[rfReplaceAll, rfIgnoreCase]);
            if Length(S)=10 then
            begin
              Result:=Copy(S,7,4)+'-'+Copy(S,1,2)+'-'+Copy(S,4,2)+' 00:00:00';
            end;
          end;
          break;
        end;
      end;
    end;
  end;
end;

function TScrapInvestment.ParsingRation(DT: AnsiString; Url: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
begin
  Result:=nil;
  if Url<>'' then
  begin
    FStrings.Text:=StringReplace(httpget(url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetration']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if (pos('lighterGrayFont arial_11',FStrings.Strings[n])>0) then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=DT;
          Obj.S['time']:='00:00';
          Obj.S['currency']:=T2;
          Obj.S['desc']:='Ration';
          Obj.S['title']:=T1;
          Obj.S['mark']:=ParamDefE('l_11">',FStrings.Strings[n+0]);
          Obj.S['period']:='NULL';
          Obj.S['event']:='RTN';
          Obj.S['name']:=ParamDefE('"">',FStrings.Strings[n+0])+' '+Obj.S['mark'];
          Obj.S['company']:=ParamDefE('<td>',FStrings.Strings[n+1]);
          Obj.S['industry']:=ParamDefE('<td>',FStrings.Strings[n+2]);
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['company']='' then Obj.S['company']:='NULL';
          if Obj.S['industry']='' then Obj.S['industry']:='NULL';
          if Obj.S['mark']='TTM' then Obj.S['period']:='1';
          if Obj.S['mark']='5YA' then Obj.S['period']:='2';
          if Obj.S['mark']='ANN' then Obj.S['period']:='3';
          if Obj.S['mark']='MRQ' then Obj.S['period']:='4';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          if (Obj.S['name']<>'') and (Obj.S['mark']<>'') then Result.A['response.widgetration'].Add(Obj);
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingEarningEx(Url1: AnsiString; Url2: AnsiString; Id: Integer): ISuperObject;
const

 DEEP_HOLE = 4;

var

  n: Integer;
  T1,T2: AnsiString;
  i: Integer;
  Obj: ISuperObject;
  lt: UInt64;
  q: Integer;

begin
  Result:=ParsingEarning(Url1,lt);
  if Assigned(Result) then
  begin
    for q:=0 to DEEP_HOLE do
    begin
      FStrings.Text:=StringReplace(HttpPostZ(Url2,Id,lt),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
      if FStrings.Count>1 then
      begin
        for n:=0 to FStrings.Count-1 do
        begin
          if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
          begin
            T1:=ParamDef(FStrings.Strings[n]);
            T2:=ParamSkb(FStrings.Strings[n]);
            Break;
          end;
        end;
        for n:=0 to FStrings.Count-1 do
        begin
          if (pos('#',FStrings.Strings[n])=0) and (pos('instrumentEarningsHistory',FStrings.Strings[n])>0) then
          begin
            lt:=StrToUnix(ParamGet('event_timestamp',FStrings.Strings[n+0])+' 00:00:00');
            Obj:=SO;
            Obj.S['timestamp']:=ParamGet('event_timestamp',FStrings.Strings[n+0])+' 00:00:00';
            Obj.S['time']:='00:00';
            if T2<>'' then
            begin
              Obj.S['currency']:=T2;
            end
            else
            begin
              case id of
                277: Obj.S['currency']:='MMM';
                8182: Obj.S['currency']:='AXP';
                6408: Obj.S['currency']:='AAPL';
                271: Obj.S['currency']:='KO';
              else
                Obj.S['currency']:='NULL';
              end;
            end;
            Obj.S['desc']:='Earning';
            Obj.S['title']:=T1;
            Obj.S['period']:='NULL';
            Obj.S['event']:='ERN';
            Obj.S['periodend']:=CorrectMySqlShort(ParamDef(FStrings.Strings[n+2]));
            Obj.S['eps']:=ParamDef(FStrings.Strings[n+3]);
            Obj.S['epsforecast']:=ParamDef(FStrings.Strings[n+4]);
            Obj.S['revenue']:=ParamDef(FStrings.Strings[n+5]);
            Obj.S['revenueforecast']:=ParamDef(FStrings.Strings[n+6]);
            Obj.S['revenuepow']:='0';
            Obj.S['forecastpow']:='0';
            if pos('B',Obj.S['revenue'])>0 then Obj.S['revenuepow']:='9';
            if pos('M',Obj.S['revenue'])>0 then Obj.S['revenuepow']:='6';
            if pos('B',Obj.S['revenueforecast'])>0 then Obj.S['forecastpow']:='9';
            if pos('M',Obj.S['revenueforecast'])>0 then Obj.S['forecastpow']:='6';
            if Obj.S['currency']='' then Obj.S['currency']:='NULL';
            if Obj.S['desc']='' then Obj.S['desc']:='NULL';
            if Obj.S['title']='' then Obj.S['title']:='NULL';
            if Obj.S['periodend']='' then Obj.S['periodend']:='NULL';
            if Obj.S['eps']='' then Obj.S['eps']:='NULL';
            if Obj.S['epsforecast']='' then Obj.S['epsforecast']:='NULL';
            if Obj.S['revenue']='' then Obj.S['revenue']:='NULL';
            if Obj.S['revenueforecast']='' then Obj.S['revenueforecast']:='NULL';
            if Obj.S['revenuepow']='' then Obj.S['revenuepow']:='NULL';
            if Obj.S['forecastpow']='' then Obj.S['forecastpow']:='NULL';
            if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
            Result.A['response.widgetearning'].Add(Obj);
          end;
        end;
      end;
    end;
  end;
end;

function TScrapInvestment.ParsingEarning(Url: AnsiString;var lt: UInt64): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
begin
  Result:=nil;
  lt:=0;
  if Url<>'' then
  begin
    FStrings.Text:=StringReplace(httpget(url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetearning']:=SO('[]');
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;

      for n:=0 to FStrings.Count-1 do
      begin
        if (pos('#',FStrings.Strings[n])=0) and (pos('instrumentEarningsHistory',FStrings.Strings[n])>0) then
        begin
          lt:=StrToUnix(ParamGet('event_timestamp',FStrings.Strings[n+0])+' 00:00:00');
          Obj:=SO;
          Obj.S['timestamp']:=ParamGet('event_timestamp',FStrings.Strings[n+0])+' 00:00:00';
          Obj.S['time']:='00:00';
          Obj.S['currency']:=T2;
          Obj.S['desc']:='Earning';
          Obj.S['title']:=T1;
          Obj.S['period']:='NULL';
          Obj.S['event']:='ERN';
          Obj.S['periodend']:=CorrectMySqlShort(ParamDef(FStrings.Strings[n+2]));
          Obj.S['eps']:=ParamDef(FStrings.Strings[n+3]);
          Obj.S['epsforecast']:=ParamDef(FStrings.Strings[n+4]);
          Obj.S['revenue']:=ParamDef(FStrings.Strings[n+5]);
          Obj.S['revenueforecast']:=ParamDef(FStrings.Strings[n+6]);
          Obj.S['revenuepow']:='0';
          Obj.S['forecastpow']:='0';
          if pos('B',Obj.S['revenue'])>0 then Obj.S['revenuepow']:='9';
          if pos('M',Obj.S['revenue'])>0 then Obj.S['revenuepow']:='6';
          if pos('B',Obj.S['revenueforecast'])>0 then Obj.S['forecastpow']:='9';
          if pos('M',Obj.S['revenueforecast'])>0 then Obj.S['forecastpow']:='6';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['periodend']='' then Obj.S['periodend']:='NULL';
          if Obj.S['eps']='' then Obj.S['eps']:='NULL';
          if Obj.S['epsforecast']='' then Obj.S['epsforecast']:='NULL';
          if Obj.S['revenue']='' then Obj.S['revenue']:='NULL';
          if Obj.S['revenueforecast']='' then Obj.S['revenueforecast']:='NULL';
          if Obj.S['revenuepow']='' then Obj.S['revenuepow']:='NULL';
          if Obj.S['forecastpow']='' then Obj.S['forecastpow']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetearning'].Add(Obj);
        end;
      end;

    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.ParsingCashflow(Url: AnsiString; Cur: AnsiString): ISuperObject;
var
  Html: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2,P: AnsiString;
  Tmp: ISuperObject;
begin
  Result:=nil;
  if Url<>'' then
  begin
    if Pos('://',Url)>0 then
    begin
      FStrings.Text:=StringReplace(httpget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      FStrings.Text:=StringReplace(fileget(Url),'</td> ','</td>'#$0A,[rfReplaceAll, rfIgnoreCase]);
    end;
    if FStrings.Count>1 then
    begin
      Result:=SO;
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
      Result.O['response.widgetcashflow']:=SO('[]');
      Tmp:=SO['[]'];
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('Period Ending:',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+1])+'/'+ParamDefE('_11">',FStrings.Strings[n+1])+' 00:00:00');
          Obj.S['le']:=ParamDefE('nt">',FStrings.Strings[n+8]);
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+2])+'/'+ParamDefE('_11">',FStrings.Strings[n+2])+' 00:00:00');
          Obj.S['le']:=ParamDefE('nt">',FStrings.Strings[n+9]);
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+3])+'/'+ParamDefE('_11">',FStrings.Strings[n+3])+' 00:00:00');
          Obj.S['le']:=ParamDefE('nt">',FStrings.Strings[n+10]);
          Tmp.AsArray.Add(Obj);
          Obj:=SO;
          Obj.S['pe']:=CorrectMySql(ParamDefE('ld">',FStrings.Strings[n+4])+'/'+ParamDefE('_11">',FStrings.Strings[n+4])+' 00:00:00');
          Obj.S['le']:=ParamDefE('nt">',FStrings.Strings[n+11]);
          Tmp.AsArray.Add(Obj);
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('float_lang_base_1 relativeAttr',FStrings.Strings[n])>0 then
        begin
          T1:=ParamDef(FStrings.Strings[n]);
          T2:=ParamSkb(FStrings.Strings[n]);
          Break;
        end;
      end;
      P:='1';
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('class="newBtn toggleButton LightGray toggled">Annual</a>',FStrings.Strings[n])>0 then
        begin
          P:='0';
          Break;
        end;
      end;
      for n:=0 to FStrings.Count-1 do
      begin
        if pos('<td><span class="',FStrings.Strings[n])>0 then
        begin
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Cashflow';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='CFW';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[0].S['pe'];
          Obj.S['le']:=Tmp.AsArray.O[0].S['le'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+1]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['le']='' then Obj.S['le']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetcashflow'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Cashflow';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='CFW';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[1].S['pe'];
          Obj.S['le']:=Tmp.AsArray.O[1].S['le'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+2]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['le']='' then Obj.S['le']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetcashflow'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Cashflow';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='CFW';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[2].S['pe'];
          Obj.S['le']:=Tmp.AsArray.O[2].S['le'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+3]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['le']='' then Obj.S['le']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetcashflow'].Add(Obj);
          Obj:=SO;
          Obj.S['timestamp']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['time']:='00:00';
          if T2<>'' then Obj.S['currency']:=T2 else Obj.S['currency']:=CUR;
          Obj.S['desc']:='Cashflow';
          Obj.S['title']:=T1;
          if (Pos('ajax',Url)>0) then Obj.S['period']:='0' else Obj.S['period']:=P;
          Obj.S['event']:='CFW';
          Obj.S['nm']:=ParamDefE('">',FStrings.Strings[n]);
          Obj.S['pe']:=Tmp.AsArray.O[3].S['pe'];
          Obj.S['le']:=Tmp.AsArray.O[3].S['le'];
          Obj.S['vl']:=ParamDef(FStrings.Strings[n+4]);
          if Obj.S['timestamp']='' then Obj.S['timestamp']:='NULL';
          if Obj.S['time']='' then Obj.S['time']:='NULL';
          if Obj.S['title']='' then Obj.S['title']:='NULL';
          if Obj.S['desc']='' then Obj.S['desc']:='NULL';
          if Obj.S['nm']='' then Obj.S['nm']:='NULL';
          if Obj.S['pe']='' then Obj.S['pe']:='NULL';
          if Obj.S['le']='' then Obj.S['le']:='NULL';
          if Obj.S['vl']='' then Obj.S['vl']:='NULL';
          if Obj.S['currency']='' then Obj.S['currency']:='NULL';
          if Obj.S['hash']='' then Obj.S['hash']:=GetHash(Obj.S['timestamp']+Obj.S['event']+Obj.S['currency']);
          Result.A['response.widgetcashflow'].Add(Obj);
        end;
      end;
    end
    else
    begin
      if FStrings.Count=1 then
      begin
        Result:=SO;
        Result.B['status']:=False;
        Result.I['code']:=403;
        Result.S['description']:=FStrings.Strings[0];
      end;
    end;
  end
  else
  begin
    Result:=SO;
    Result.B['status']:=False;
    Result.I['code']:=403;
    Result.S['description']:='Url not found';
  end;
end;

function TScrapInvestment.DoGet(Url: ISuperObject): ISuperObject;
var
  Str: AnsiString;
  n,i: UInt64;
  a: Integer;
  Obj: ISuperObject;
  Uri: AnsiString;
  T1,T2: AnsiString;
  Tst: ISuperObject;
  F: UInt64;
  T: UInt64;
  Url1: AnsiString;
  Url2: AnsiString;
  lt: UInt64;
begin

  if Url.S['obj.link.path']='/query/financialsummary' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-financial-summary';
      Url2:='https://www.investing.com/instruments/Financials/changesummaryreporttypeajax?action=change_report_type&pid=277&financial_id=277&ratios_id=277&period_type=Annual';
      Result:=ParsingFinancialSummary(Url1,Url2);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-financial-summary';
      Url2:='https://www.investing.com/instruments/Financials/changesummaryreporttypeajax?action=change_report_type&pid=8182&financial_id=8182&ratios_id=8182&period_type=Annual';
      Result:=ParsingFinancialSummary(Url1,Url2);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-financial-summary';
      Url2:='https://www.investing.com/instruments/Financials/changesummaryreporttypeajax?action=change_report_type&pid=6408&financial_id=6408&ratios_id=6408&period_type=Annual';
      Result:=ParsingFinancialSummary(Url1,Url2);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-financial-summary';
      Url2:='https://www.investing.com/instruments/Financials/changesummaryreporttypeajax?action=change_report_type&pid=271&financial_id=271&ratios_id=271&period_type=Annual';
      Result:=ParsingFinancialSummary(Url1,Url2);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/summaryincomestatement' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=INC&period_type=Annual';
      Result:=ParsingSummaryIncomeStatement(Url1,'MMM');
      CopyArray(Result.A['response.widgetsummaryincomestatement'],ParsingSummaryIncomeStatement(url2,'MMM').A['response.widgetsummaryincomestatement']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=INC&period_type=Annual';
      Result:=ParsingSummaryIncomeStatement(Url1,'AXP');
      CopyArray(Result.A['response.widgetsummaryincomestatement'],ParsingSummaryIncomeStatement(url2,'AXP').A['response.widgetsummaryincomestatement']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=INC&period_type=Annual';
      Result:=ParsingSummaryIncomeStatement(Url1,'AAPL');
      CopyArray(Result.A['response.widgetsummaryincomestatement'],ParsingSummaryIncomeStatement(url2,'AAPL').A['response.widgetsummaryincomestatement']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=INC&period_type=Annual';
      Result:=ParsingSummaryIncomeStatement('https://www.investing.com/equities/coca-cola-co-income-statement','KO');
      CopyArray(Result.A['response.widgetsummaryincomestatement'],ParsingSummaryIncomeStatement(Url2,'KO').A['response.widgetsummaryincomestatement']);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/summarybalancesheet' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/american-express-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=BAL&period_type=Annual';
      Result:=ParsingSummaryBalanceSheet(Url1,'MMM');
      CopyArray(Result.A['response.widgetsummarybalancesheet'],ParsingSummaryBalanceSheet(Url2,'MMM').A['response.widgetsummarybalancesheet']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=BAL&period_type=Annual';
      Result:=ParsingSummaryBalanceSheet(Url1,'AXP');
      CopyArray(Result.A['response.widgetsummarybalancesheet'],ParsingSummaryBalanceSheet(Url2,'AXP').A['response.widgetsummarybalancesheet']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=BAL&period_type=Annual';
      Result:=ParsingSummaryBalanceSheet(Url1,'AAPL');
      CopyArray(Result.A['response.widgetsummarybalancesheet'],ParsingSummaryBalanceSheet(Url2,'AAPL').A['response.widgetsummarybalancesheet']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=BAL&period_type=Annual';
      Result:=ParsingSummaryBalanceSheet(Url1,'KO');
      CopyArray(Result.A['response.widgetsummarybalancesheet'],ParsingSummaryBalanceSheet(Url2,'KO').A['response.widgetsummarybalancesheet']);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/incomestatement' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=INC&period_type=Annual';
      Result:=ParsingIncomeStatement(Url1,'MMM');
      CopyArray(Result.A['response.widgetincomestatement'],ParsingIncomeStatement(Url2,'MMM').A['response.widgetincomestatement']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=INC&period_type=Annual';
      Result:=ParsingIncomeStatement(Url1,'AXP');
      CopyArray(Result.A['response.widgetincomestatement'],ParsingIncomeStatement(Url2,'AXP').A['response.widgetincomestatement']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=INC&period_type=Annual';
      Result:=ParsingIncomeStatement(Url1,'AAPL');
      CopyArray(Result.A['response.widgetincomestatement'],ParsingIncomeStatement(Url2,'AAPL').A['response.widgetincomestatement']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-income-statement';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=INC&period_type=Annual';
      Result:=ParsingIncomeStatement(Url1,'KO');
      CopyArray(Result.A['response.widgetincomestatement'],ParsingIncomeStatement(Url2,'KO').A['response.widgetincomestatement']);
    end;
    Exit;
  end;

  //REVIEW

  if Url.S['obj.link.path']='/query/balancesheet' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=BAL&period_type=Annual';
      Result:=ParsingBalanceSheet(Url1,'MMM');
      CopyArray(Result.A['response.widgetbalancesheet'],ParsingBalanceSheet(Url2,'MMM').A['response.widgetbalancesheet']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=BAL&period_type=Annual';
      Result:=ParsingBalanceSheet(Url1,'AXP');
      CopyArray(Result.A['response.widgetbalancesheet'],ParsingBalanceSheet(Url2,'AXP').A['response.widgetbalancesheet']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=BAL&period_type=Annual';
      Result:=ParsingBalanceSheet(Url1,'AAPL');
      CopyArray(Result.A['response.widgetbalancesheet'],ParsingBalanceSheet(Url2,'AAPL').A['response.widgetbalancesheet']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-balance-sheet';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=BAL&period_type=Annual';
      Result:=ParsingBalanceSheet(Url1,'KO');
      CopyArray(Result.A['response.widgetbalancesheet'],ParsingBalanceSheet(Url2,'KO').A['response.widgetbalancesheet']);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/summarycashflowstatement' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=CAS&period_type=Annual';
      Result:=ParsingSummaryCashFlowStatement(Url1,'MMM');
      CopyArray(Result.A['response.widgetcashstatement'],ParsingSummaryCashFlowStatement(Url2,'MMM').A['response.widgetcashstatement']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=CAS&period_type=Annual';
      Result:=ParsingSummaryCashFlowStatement(Url1,'AXP');
      CopyArray(Result.A['response.widgetcashstatement'],ParsingSummaryCashFlowStatement(Url2,'AXP').A['response.widgetcashstatement']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=CAS&period_type=Annual';
      Result:=ParsingSummaryCashFlowStatement(Url1,'AAPL');
      CopyArray(Result.A['response.widgetcashstatement'],ParsingSummaryCashFlowStatement(Url2,'AAPL').A['response.widgetcashstatement']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=CAS&period_type=Annual';
      Result:=ParsingSummaryCashFlowStatement(Url1,'KO');
      CopyArray(Result.A['response.widgetcashstatement'],ParsingSummaryCashFlowStatement(Url2,'KO').A['response.widgetcashstatement']);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/centralbankrates' then
  begin
    if Url.S['obj.link.value.name']='FED' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-168';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,168);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='ECB' then
    begin
      Url1:='ttps://www.investing.com/economic-calendar/interest-rate-decision-164';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,164);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='RBA' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-171';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,171);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='BOJ' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-165';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,165);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='BOC' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-166';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,166);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='SNB' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-169';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,169);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='RBNZ' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-167';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,167);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='BOE' then
    begin
      Url1:='https://www.investing.com/economic-calendar/interest-rate-decision-171';
      Url2:='https://www.investing.com/economic-calendar/more-history';
      Result:=ParsingCentralBanksRateEx(Url1,Url2,171);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetcentralbankrate'])=True) then
        begin
          for n:=0 to Result.A['response.widgetcentralbankrate'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetcentralbankrate'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetcentralbankrate'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/dividents' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-dividends';
      Url2:='https://www.investing.com/equities/MoreDividendsHistory';
      Result:=ParsingDividendsEx(Url1,Url2,277);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetdividends'])=True) then
        begin
          for n:=0 to Result.A['response.widgetdividends'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetdividends'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-dividends';
      Url2:='https://www.investing.com/equities/MoreDividendsHistory';
      Result:=ParsingDividendsEx(Url1,Url2,8182);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetdividends'])=True) then
        begin
          for n:=0 to Result.A['response.widgetdividends'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetdividends'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-dividends';
      Url2:='https://www.investing.com/equities/MoreDividendsHistory';
      Result:=ParsingDividendsEx(Url1,Url2,6408);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetdividends'])=True) then
        begin
          for n:=0 to Result.A['response.widgetdividends'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetdividends'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-dividends';
      Url2:='https://www.investing.com/equities/MoreDividendsHistory';
      Result:=ParsingDividendsEx(Url1,Url2,271);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetdividends'])=True) then
        begin
          for n:=0 to Result.A['response.widgetdividends'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetdividends'].O[n].S['timestamp'])<=T) then
            begin
              //
            end
            else
            begin
              Result.A['response.widgetdividends'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    Exit;
  end;

  if Url.S['obj.link.path']='/query/ration' then
  begin
    if Url.S['obj.link.value.name']='MMM' then Result:=ParsingRation(ParsingLatestRelease('https://www.investing.com/equities/3m-co-earnings'),'https://www.investing.com/equities/3m-co-ratios');
    if Url.S['obj.link.value.name']='AXP' then Result:=ParsingRation(ParsingLatestRelease('https://www.investing.com/equities/american-express-earnings'),'https://www.investing.com/equities/american-express-ratios');
    if Url.S['obj.link.value.name']='AAPL' then Result:=ParsingRation(ParsingLatestRelease('https://www.investing.com/equities/apple-computer-inc-earnings'),'https://www.investing.com/equities/apple-computer-inc-ratios');
    if Url.S['obj.link.value.name']='KO' then Result:=ParsingRation(ParsingLatestRelease('https://www.investing.com/equities/coca-cola-co-earnings'),'https://www.investing.com/equities/coca-cola-co-ratios');
    Exit;
  end;

  if Url.S['obj.link.path']='/query/earning' then
  begin

    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-earnings';
      Url2:='https://www.investing.com/equities/morehistory';
      Result:=ParsingEarningEx(Url1,Url2,277);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetearning'])=True) then
        begin
          for n:=0 to Result.A['response.widgetearning'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])<=T) then
            begin

            end
            else
            begin
              Result.A['response.widgetearning'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-earnings';
      Url2:='https://www.investing.com/equities/morehistory';
      Result:=ParsingEarningEx(Url1,Url2,8182);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetearning'])=True) then
        begin
          for n:=0 to Result.A['response.widgetearning'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])<=T) then
            begin

            end
            else
            begin
              Result.A['response.widgetearning'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-earnings';
      Url2:='https://www.investing.com/equities/morehistory';
      Result:=ParsingEarningEx(Url1,Url2,6408);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetearning'])=True) then
        begin
          for n:=0 to Result.A['response.widgetearning'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])<=T) then
            begin

            end
            else
            begin
              Result.A['response.widgetearning'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-earnings';
      Url2:='https://www.investing.com/equities/morehistory';
      Result:=ParsingEarningEx(Url1,Url2,271);
      if (Url.S['obj.link.value.from']<>'') and (Url.S['obj.link.value.to']<>'') then
      begin
        F:=StrToUnix(Url.S['obj.link.value.from']);
        T:=StrToUnix(Url.S['obj.link.value.to']);
        if (Assigned(Result)=True) and
           (Assigned(Result.O['response.widgetearning'])=True) then
        begin
          for n:=0 to Result.A['response.widgetearning'].Length-1 do
          begin
            if (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])>=F) and
               (StrToUnix(Result.A['response.widgetearning'].O[n].S['timestamp'])<=T) then
            begin

            end
            else
            begin
              Result.A['response.widgetearning'].O[n].Clear;
            end;
          end;
        end;
      end;
    end;

    Exit;
  end;

  if Url.S['obj.link.path']='/query/economiccalendar' then
  begin
    if LowerCase(Url.S['obj.link.value.avoid'])='true' then
    begin
      Result:=ParsingEconomicCalendar('https://sslecal2.investing.com/ajax.php',Url.S['obj.link.value.from'],Url.S['obj.link.value.to'],True, Url.I['obj.link.value.importance']);
    end
    else
    begin
      Result:=ParsingEconomicCalendar('https://sslecal2.investing.com/ajax.php',Url.S['obj.link.value.from'],Url.S['obj.link.value.to'],False,Url.I['obj.link.value.importance']);
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/query/cashflow' then
  begin
    if Url.S['obj.link.value.name']='MMM' then
    begin
      Url1:='https://www.investing.com/equities/3m-co-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=277&report_type=CAS&period_type=Annual';
      Result:=ParsingCashFlow(Url1,'MMM');
      CopyArray(Result.A['response.widgetcashflow'],ParsingCashFlow(Url2,'MMM').A['response.widgetcashflow']);
    end;
    if Url.S['obj.link.value.name']='AXP' then
    begin
      Url1:='https://www.investing.com/equities/american-express-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=8182&report_type=CAS&period_type=Annual';
      Result:=ParsingCashFlow(Url1,'AXP');
      CopyArray(Result.A['response.widgetcashflow'],ParsingCashFlow(Url2,'AXP').A['response.widgetcashflow']);
    end;
    if Url.S['obj.link.value.name']='AAPL' then
    begin
      Url1:='https://www.investing.com/equities/apple-computer-inc-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=6408&report_type=CAS&period_type=Annual';
      Result:=ParsingCashFlow(Url1,'AAPL');
      CopyArray(Result.A['response.widgetcashflow'],ParsingCashFlow(Url2,'AAPL').A['response.widgetcashflow']);
    end;
    if Url.S['obj.link.value.name']='KO' then
    begin
      Url1:='https://www.investing.com/equities/coca-cola-co-cash-flow';
      Url2:='https://www.investing.com/instruments/Financials/changereporttypeajax?action=change_report_type&pair_ID=271&report_type=CAS&period_type=Annual';
      Result:=ParsingCashFlow(Url1,'KO');
      CopyArray(Result.A['response.widgetcashflow'],ParsingCashFlow(Url2,'KO').A['response.widgetcashflow']);
    end;
    Exit;
  end;


  Result:=SO;
  Result.B['status']:=False;
  Result.I['code']:=403;
  Result.S['description']:='Invalid query';
end;

function TScrapInvestment.DoPost(Url: ISuperObject; Body: ISuperObject): ISuperObject;
begin
  Result:=nil;
end;

{TPushMysqlDatabase}

procedure TPushMysqlDatabase.DoReset;
begin
  FhMySql:=nil;
  Fhstmt:=nil;
  FhRes:=nil;
  FhRow:=nil;
  FAddress:='127.0.0.1';
  FPort:=3306;
  FLogin:='root';
  FPassword:='root';
  FScheme:='scrap';
  FHashlist:=nil;
  FHashControl:=False;
end;

function TPushMysqlDatabase.Correct(V: AnsiString): AnsiString;
var i,c: Integer;
begin
  c:=0;
  Result:='';
  if V<>'' then
  begin
    i:=pos('%',V);
    if i>0 then Delete(V,i,1);
    i:=pos('M',V);
    if i>0 then Delete(V,i,1);
    i:=pos('B',V);
    if i>0 then Delete(V,i,1);
    i:=pos('K',V);
    if i>0 then Delete(V,i,1);
    Result:=StringReplace(V,',','.', [rfReplaceAll, rfIgnoreCase]);
    for i:=1 to Length(Result) do
    begin
      if Result[i]='.' then c:=c+1;
    end;
    i:=pos('.',Result);
    if (c=2) and (i>0) then Delete(Result,i,1);
    Result:=Trim(Result);
    if (Length(Result)=1) and (Result[1]='-') then Result:='';
  end;
end;

function TPushMysqlDatabase.CorrectD(V: AnsiString): AnsiString;
begin
  Result:=StringReplace(V,'(','', [rfReplaceAll, rfIgnoreCase]);
  Result:=StringReplace(Result,')','', [rfReplaceAll, rfIgnoreCase]);
  Result:=StringReplace(Result,',','', [rfReplaceAll, rfIgnoreCase]);
  Result:=StringReplace(Result,',','', [rfReplaceAll, rfIgnoreCase]);
end;

procedure TPushMysqlDatabase.SetAddress(V: AnsiString);
begin
  FAddress:=Trim(V);
end;

procedure TPushMysqlDatabase.SetPort(V: Word);
begin
  FPort:=V;
end;

procedure TPushMysqlDatabase.SetLogin(V: AnsiString);
begin
  FLogin:=Trim(V);
end;

procedure TPushMysqlDatabase.SetPassword(V: AnsiString);
begin
  FPassword:=Trim(V);
end;

procedure TPushMysqlDatabase.SetScheme(V: AnsiString);
begin
  FScheme:=Trim(V);
end;

procedure TPushMysqlDatabase.SetControl(V: Boolean);
begin
  FHashControl:=V;
  FHashlist.Clear;
end;

function TPushMysqlDatabase.GetFieldIncomestatement(Name: AnsiString): UInt64;
var Sql: AnsiString;
    Count: UInt64;
begin
  Result:=0;
  Sql:='SELECT * FROM incomestatement_fields WHERE fieldname="'+Trim(Name)+'";';
  if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
  begin
    FhRes:=mysql_store_result(FhMySql);
    if Assigned(FhRes) then
    begin
      Count:=mysql_num_rows(FhRes);
      if Count>0 then
      begin
        //
      end
      else
      begin
        Sql:='insert into incomestatement_fields (';
        Sql:=Sql+'fieldname';
        Sql:=Sql+') values (';
        Sql:=Sql+'"'+Trim(Name)+'"';
        Sql:=Sql+');';
        mysql_query(FhMySql, PAnsiChar(Sql));
      end;
      Sql:='SELECT * FROM incomestatement_fields WHERE fieldname="'+Trim(Name)+'";';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            Result:=StrToInt(FhRow^[0]);
          end;
        end;
      end;
    end;
  end;
end;

function TPushMysqlDatabase.GetFieldBalanceSheet(Name: AnsiString): UInt64;
var Sql: AnsiString;
    Count: UInt64;
begin
  Result:=0;
  Sql:='SELECT * FROM balancesheet_fields WHERE fieldname="'+Trim(Name)+'";';
  if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
  begin
    FhRes:=mysql_store_result(FhMySql);
    if Assigned(FhRes) then
    begin
      Count:=mysql_num_rows(FhRes);
      if Count>0 then
      begin
        //
      end
      else
      begin
        Sql:='insert into balancesheet_fields (';
        Sql:=Sql+'fieldname';
        Sql:=Sql+') values (';
        Sql:=Sql+'"'+Trim(Name)+'"';
        Sql:=Sql+');';
        mysql_query(FhMySql, PAnsiChar(Sql));
      end;
      Sql:='SELECT * FROM balancesheet_fields WHERE fieldname="'+Trim(Name)+'";';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            Result:=StrToInt(FhRow^[0]);
          end;
        end;
      end;
    end;
  end;
end;

function TPushMysqlDatabase.GetFieldRation(Name: AnsiString): UInt64;
var Sql: AnsiString;
    Count: UInt64;
begin
  Result:=0;
  Sql:='SELECT * FROM ration_fields WHERE fieldname="'+Trim(Name)+'";';
  if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
  begin
    FhRes:=mysql_store_result(FhMySql);
    if Assigned(FhRes) then
    begin
      Count:=mysql_num_rows(FhRes);
      if Count>0 then
      begin
        //
      end
      else
      begin
        Sql:='insert into ration_fields (';
        Sql:=Sql+'fieldname';
        Sql:=Sql+') values (';
        Sql:=Sql+'"'+Trim(Name)+'"';
        Sql:=Sql+');';
        mysql_query(FhMySql, PAnsiChar(Sql));
      end;
      Sql:='SELECT * FROM ration_fields WHERE fieldname="'+Trim(Name)+'";';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            Result:=StrToInt(FhRow^[0]);
          end;
        end;
      end;
    end;
  end;
end;

function TPushMysqlDatabase.GetFieldCashflow(Name: AnsiString): UInt64;
var Sql: AnsiString;
    Count: UInt64;
begin
  Result:=0;
  Sql:='SELECT * FROM cashflow_fields WHERE fieldname="'+Trim(Name)+'";';
  if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
  begin
    FhRes:=mysql_store_result(FhMySql);
    if Assigned(FhRes) then
    begin
      Count:=mysql_num_rows(FhRes);
      if Count>0 then
      begin
        //
      end
      else
      begin
        Sql:='insert into cashflow_fields (';
        Sql:=Sql+'fieldname';
        Sql:=Sql+') values (';
        Sql:=Sql+'"'+Trim(Name)+'"';
        Sql:=Sql+');';
        mysql_query(FhMySql, PAnsiChar(Sql));
      end;
      Sql:='SELECT * FROM cashflow_fields WHERE fieldname="'+Trim(Name)+'";';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            Result:=StrToInt(FhRow^[0]);
          end;
        end;
      end;
    end;
  end;
end;

function TPushMysqlDatabase.GetFieldDivident(TP: AnsiString; Name: AnsiString): UInt64;
var Sql: AnsiString;
    Count: UInt64;
begin
  Result:=0;
  Sql:='SELECT * FROM divident_types WHERE description="'+Trim(Name)+'";';
  if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
  begin
    FhRes:=mysql_store_result(FhMySql);
    if Assigned(FhRes) then
    begin
      Count:=mysql_num_rows(FhRes);
      if Count>0 then
      begin
        //
      end
      else
      begin
        Sql:='insert into divident_types (';
        Sql:=Sql+'symbol,description';
        Sql:=Sql+') values (';
        Sql:=Sql+'"'+Trim(TP)+'",';
        Sql:=Sql+'"'+Trim(Name)+'"';
        Sql:=Sql+');';
        mysql_query(FhMySql, PAnsiChar(Sql));
      end;
      Sql:='SELECT * FROM divident_types WHERE description="'+Trim(Name)+'";';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        FhRes:=mysql_store_result(FhMySql);
        if Assigned(FhRes) then
        begin
          Count:=mysql_num_rows(FhRes);
          if Count>0 then
          begin
            Count:=0;
            mysql_data_seek(FhRes,Count);
            FhRow:=mysql_fetch_row(FhRes);
            Result:=StrToInt(FhRow^[0]);
          end;
        end;
      end;
    end;
  end;
end;

function TPushMysqlDatabase.DoActivate: Boolean;
var Sql: AnsiString;
begin
  Result:=False;
  FHashlist:=TStringList.Create;
  libmysql_load(nil);
  FhMySql:=mysql_init(nil);
  if Assigned(FhMySql) then
  begin
    FhMySql:=mysql_real_connect(FhMySql,PAnsiChar(Address),PAnsiChar(FLogin), PAnsiChar(FPassword),PAnsiChar(FScheme),FPort, nil, 0);
    if Assigned(FhMySql) then
    begin

      Sql:='CREATE TABLE IF NOT EXISTS economic_calendar (';
      Sql:=Sql+'time DATETIME NULL,';
      Sql:=Sql+'currency CHAR(4) NULL,';
      Sql:=Sql+'importance TINYINT(3) NULL,';
      Sql:=Sql+'event VARCHAR(50) NULL,';
      Sql:=Sql+'actual DOUBLE NULL,';
      Sql:=Sql+'forecast DOUBLE NULL,';
      Sql:=Sql+'previous DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS central_banks_rates (';
      Sql:=Sql+'time DATETIME NULL,';
      Sql:=Sql+'bank CHAR(4) NULL,';
      Sql:=Sql+'actual DOUBLE NULL,';
      Sql:=Sql+'forecast DOUBLE NULL,';
      Sql:=Sql+'previous DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS summary (';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'txt TEXT NULL,';
      Sql:=Sql+'grossmargin DOUBLE NULL,';
      Sql:=Sql+'operatingmargin DOUBLE NULL,';
      Sql:=Sql+'netprofitmargin DOUBLE NULL,';
      Sql:=Sql+'returnoninversment DOUBLE NULL,';
      Sql:=Sql+'quickration DOUBLE NULL,';
      Sql:=Sql+'currentratio DOUBLE NULL,';
      Sql:=Sql+'ltdebttoequity DOUBLE NULL,';
      Sql:=Sql+'totaldebttoequity DOUBLE NULL,';
      Sql:=Sql+'cashflowshare DOUBLE NULL,';
      Sql:=Sql+'revenueshare DOUBLE NULL,';
      Sql:=Sql+'annualoperatingcashflow DOUBLE NULL,';
      Sql:=Sql+'quarterlyoperatingcashflow DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS summary_incomestatement (';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'totalrevenue INT(8) NULL,';
      Sql:=Sql+'grossprofit INT(8) NULL,';
      Sql:=Sql+'operatingincome INT(8) NULL,';
      Sql:=Sql+'netincome INT(8) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS summary_balancesheet (';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'totalassets INT(8) NULL,';
      Sql:=Sql+'totalliabilities INT(8) NULL,';
      Sql:=Sql+'totalequity INT(8) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS summary_cashflowstatement  (';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'cashfromoperatingactivities INT(8) NULL,';
      Sql:=Sql+'cashfrominvestingactivities INT(8) NULL,';
      Sql:=Sql+'cashfromfinancingactivities INT(8) NULL,';
      Sql:=Sql+'netchangeincash INT(8) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS incomestatement_fields (';
      Sql:=Sql+'fieldid INT unsigned NOT NULL AUTO_INCREMENT,';
      Sql:=Sql+'primary key (fieldid),';
      Sql:=Sql+'fieldname CHAR(60) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS incomestatement (';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'fieldid TINYINT(3) NULL,';
      Sql:=Sql+'value DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS balancesheet_fields (';
      Sql:=Sql+'fieldid INT unsigned NOT NULL AUTO_INCREMENT,';
      Sql:=Sql+'primary key (fieldid),';
      Sql:=Sql+'fieldname CHAR(60) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS balancesheet (';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'fieldid TINYINT(3) NULL,';
      Sql:=Sql+'value DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS divident_types (';
      Sql:=Sql+'fieldid INT unsigned NOT NULL AUTO_INCREMENT,';
      Sql:=Sql+'primary key (fieldid),';
      Sql:=Sql+'symbol CHAR(3) NULL,';
      Sql:=Sql+'description CHAR(60) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS dividents (';
      Sql:=Sql+'symbol CHAR(10) NULL,';
      Sql:=Sql+'exdividentdate DATETIME NULL,';
      Sql:=Sql+'divident DOUBLE NULL,';
      Sql:=Sql+'type TINYINT(3) NULL,';
      Sql:=Sql+'paymentdate DATETIME NULL,';
      Sql:=Sql+'yield DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS ration_fields (';
      Sql:=Sql+'fieldid INT unsigned NOT NULL AUTO_INCREMENT,';
      Sql:=Sql+'primary key (fieldid),';
      Sql:=Sql+'fieldname CHAR(60) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS ratios (';
      Sql:=Sql+'symbol CHAR(10),';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'fieldid INT NULL,';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'company DOUBLE NULL,';
      Sql:=Sql+'industry DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS earnings (';
      Sql:=Sql+'symbol CHAR(10),';
      Sql:=Sql+'releasedate DATETIME NULL,';
      Sql:=Sql+'periodend DATETIME NULL,';
      Sql:=Sql+'eps DOUBLE NULL,';
      Sql:=Sql+'epsforecast DOUBLE NULL,';
      Sql:=Sql+'revenue DOUBLE NULL,';
      Sql:=Sql+'revenueforecast DOUBLE NULL,';
      Sql:=Sql+'revenuepow TINYINT(3) NULL,';
      Sql:=Sql+'forecastpow TINYINT(3) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS cashflow_fields (';
      Sql:=Sql+'fieldid INT unsigned NOT NULL AUTO_INCREMENT,';
      Sql:=Sql+'primary key (fieldid),';
      Sql:=Sql+'fieldname CHAR(60) NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;

      Sql:='CREATE TABLE IF NOT EXISTS cashflow (';
      Sql:=Sql+'symbol CHAR(10),';
      Sql:=Sql+'period TINYINT(3) NULL,';
      Sql:=Sql+'periodlength TINYINT(3) NULL,';
      Sql:=Sql+'date DATETIME NULL,';
      Sql:=Sql+'fieldid INT NULL,';
      Sql:=Sql+'value DOUBLE NULL';
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then Result:=True else Result:=False;


    end;
  end;
end;

function TPushMysqlDatabase.DoDeactivate: Boolean;
begin
  FHashlist.Destroy;
  if Assigned(FhMySql) then
  begin
    mysql_close(FhMySql);
    FhMySql:=nil;
  end;
  Result:=True;
end;

function TPushMysqlDatabase.DoGet(Url: ISuperObject): ISuperObject;
begin
  Result:=nil;
end;

function TPushMysqlDatabase.DoPost(Url: ISuperObject; Body: ISuperObject): ISuperObject;
var Sql: AnsiString;
begin
  Result:=nil;
  if Url.S['obj.link.path']='/drop' then
  begin
    Result:=SO;
    Sql:='drop table ';
    Sql:=Sql+'economic_calendar,';
    Sql:=Sql+'central_banks_rates,';
    Sql:=Sql+'summary,';
    Sql:=Sql+'summary_incomestatement,';
    Sql:=Sql+'summary_balancesheet,';
    Sql:=Sql+'summary_cashflowstatement,';
    Sql:=Sql+'incomestatement_fields,';
    Sql:=Sql+'incomestatement,';
    Sql:=Sql+'balancesheet_fields,';
    Sql:=Sql+'balancesheet,';
    Sql:=Sql+'divident_types,';
    Sql:=Sql+'dividents,';
    Sql:=Sql+'ration_fields,';
    Sql:=Sql+'ratios,';
    Sql:=Sql+'earnings,';
    Sql:=Sql+'cashflow_fields,';
    Sql:=Sql+'cashflow;';
    if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
    begin
      Result.B['status']:=True;
      Result.I['code']:=0;
      Result.S['description']:='Success';
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=403;
      Result.S['description']:='Fault';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/economic_calendar' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into economic_calendar (';
      Sql:=Sql+'time,currency,importance,event,actual,forecast,previous';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+Body.S['currency']+'",';
      Sql:=Sql+'"'+Body.S['importance']+'",';
      Sql:=Sql+'"'+CorrectD(Body.S['title'])+'",';
      Sql:=Sql+Correct(Body.S['act'])+',';
      Sql:=Sql+Correct(Body.S['fore'])+',';
      Sql:=Sql+Correct(Body.S['prev']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/central_banks_rates' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into central_banks_rates (';
      Sql:=Sql+'time,bank,actual,forecast,previous';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+Body.S['currency']+'",';
      Sql:=Sql+Correct(Body.S['actual'])+',';
      Sql:=Sql+Correct(Body.S['forecast'])+',';
      Sql:=Sql+Correct(Body.S['previous']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/financial_summary' then
    begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into summary (';
      Sql:=Sql+'date,symbol,txt,grossmargin,operatingmargin,netprofitmargin,returnoninversment,quickration,currentratio,ltdebttoequity,totaldebttoequity,cashflowshare,revenueshare,annualoperatingcashflow,quarterlyoperatingcashflow';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+Body.S['currency']+'",';
      Sql:=Sql+'"'+CorrectD(Body.S['text'])+'",';
      Sql:=Sql+Correct(Body.S['grossmargin'])+',';
      Sql:=Sql+Correct(Body.S['operatingmargin'])+',';
      Sql:=Sql+Correct(Body.S['netprofitmargin'])+',';
      Sql:=Sql+Correct(Body.S['returnoninvestment'])+',';
      Sql:=Sql+Correct(Body.S['quickratio'])+',';
      Sql:=Sql+Correct(Body.S['currentratio'])+',';
      Sql:=Sql+Correct(Body.S['ltdebttoequity'])+',';
      Sql:=Sql+Correct(Body.S['totaldebttoequity'])+',';
      Sql:=Sql+Correct(Body.S['cashflowshare'])+',';
      Sql:=Sql+Correct(Body.S['revenueshare'])+',';
      Sql:=Sql+Correct(Body.S['operatingcashflow1'])+',';
      Sql:=Sql+Correct(Body.S['operatingcashflow2']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/summaryincomestatement' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into summary_incomestatement (';
      Sql:=Sql+'period,date,symbol,totalrevenue,grossprofit,operatingincome,netincome';
      Sql:=Sql+') values (';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Correct(Body.S['totalrevenue'])+',';
      Sql:=Sql+Correct(Body.S['grossprofit'])+',';
      Sql:=Sql+Correct(Body.S['operatingincome'])+',';
      Sql:=Sql+Correct(Body.S['netincome']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/summarybalancesheet' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into summary_balancesheet (';
      Sql:=Sql+'period,date,symbol,totalassets,totalliabilities,totalequity';
      Sql:=Sql+') values (';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Correct(Body.S['totalassets'])+',';
      Sql:=Sql+Correct(Body.S['totalliabilities'])+',';
      Sql:=Sql+Correct(Body.S['totalequity']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/cashflowstatement' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into summary_cashflowstatement (';
      Sql:=Sql+'period,date,symbol,cashfromoperatingactivities,cashfrominvestingactivities,cashfromfinancingactivities,netchangeincash';
      Sql:=Sql+') values (';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Correct(Body.S['cashfromoperating'])+',';
      Sql:=Sql+Correct(Body.S['cashfrominvesting'])+',';
      Sql:=Sql+Correct(Body.S['cashfromfinancing'])+',';
      Sql:=Sql+Correct(Body.S['cashnetchangein']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/dividents' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into dividents (';
      Sql:=Sql+'symbol,exdividentdate,divident,type,paymentdate,yield';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+'"'+Body.S['exdividentdate']+'",';
      Sql:=Sql+Correct(Body.S['dividend'])+',';
      Sql:=Sql+IntToStr(GetFieldDivident(Body.S['mark'],Correct(Body.S['type'])))+',';
      Sql:=Sql+'"'+Body.S['paymentdate']+'",';
      Sql:=Sql+Correct(Body.S['yield']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/ration' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into ratios (';
      Sql:=Sql+'symbol,date,fieldid,period,company,industry';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+IntToStr(GetFieldRation(Body.S['name']))+',';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+Correct(Body.S['company'])+',';
      Sql:=Sql+Correct(Body.S['industry']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
      Exit;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/earning' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into earnings (';
      Sql:=Sql+'symbol,releasedate,periodend,eps,epsforecast,revenue,revenueforecast,revenuepow,forecastpow';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+'"'+Body.S['periodend']+'",';
      Sql:=Sql+Correct(Body.S['eps'])+',';
      Sql:=Sql+Correct(Body.S['epsforecast'])+',';
      Sql:=Sql+Correct(Body.S['revenue'])+',';
      Sql:=Sql+Correct(Body.S['revenueforecast'])+',';
      Sql:=Sql+Correct(Body.S['revenuepow'])+',';
      Sql:=Sql+Correct(Body.S['forecastpow']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
      Exit;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/cashflow' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into cashflow (';
      Sql:=Sql+'symbol,period,periodlength,date,fieldid,value';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+Body.S['le']+',';
      Sql:=Sql+'"'+Body.S['timestamp']+'",';
      Sql:=Sql+IntToStr(GetFieldCashflow(Body.S['nm']))+',';
      Sql:=Sql+Correct(Body.S['vl']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
      Exit;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/balancesheet' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into balancesheet (';
      Sql:=Sql+'symbol,period,date,fieldid,value';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+'"'+Body.S['pe']+'",';
      Sql:=Sql+IntToStr(GetFieldBalanceSheet(Body.S['nm']))+',';
      Sql:=Sql+Correct(Body.S['vl']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
      Exit;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

  if Url.S['obj.link.path']='/insert/incomestatement' then
  begin
    Result:=SO;
    if FHashlist.Values[Body.S['hash']]='' then
    begin
      Sql:='insert into incomestatement (';
      Sql:=Sql+'symbol,period,date,fieldid,value';
      Sql:=Sql+') values (';
      Sql:=Sql+'"'+CorrectD(Body.S['currency'])+'",';
      Sql:=Sql+Body.S['period']+',';
      Sql:=Sql+'"'+Body.S['pe']+'",';
      Sql:=Sql+IntToStr(GetFieldIncomeStatement(Body.S['nm']))+',';
      Sql:=Sql+Correct(Body.S['vl']);
      Sql:=Sql+');';
      if mysql_query(FhMySql, PAnsiChar(Sql))=0 then
      begin
        Result.B['status']:=True;
        Result.I['code']:=0;
        Result.S['description']:='Success';
        if FHashControl=True then FHashlist.Add(Body.S['hash']+'='+Body.S['hash']);
        if (FHashControl=True) and (FHashlist.Count>HASH_MAX_COUNT) then FHashlist.Delete(0);
      end
      else
      begin
        Result.B['status']:=False;
        Result.I['code']:=-1;
        Result.S['description']:='Invalid insert record';
      end;
      Exit;
    end
    else
    begin
      Result.B['status']:=False;
      Result.I['code']:=-2;
      Result.S['description']:='Record already exists';
    end;
    Exit;
  end;

end;

end.
