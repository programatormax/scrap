program scrap;

////
///  NOT START AND NOT USE FOR TEST THIS APPLICATION!!
///

{$APPTYPE CONSOLE}

{$R *.res}

uses

  Windows,
  SysUtils,
  DateUtils,
  Classes,
  ScrapUnit,
  ShellApi,
  shlobj,
  SuperObject;

var

  Investing: TScrapInvestment;
  MySql: TPushMysqlDatabase;
  n,i: UInt64;
  Stamp1: UInt64;
  Stamp2: UInt64;
  Stamp3: UInt64;
  Stamp4: UInt64;
  Stamp5: UInt64;
  Stamp6: UInt64;
  Stamp7: UInt64;
  Stamp8: UInt64;
  Stamp9: UInt64;
  Stamp10: UInt64;

//yyyy-mm-dd hh:nn:ss

//UNKNOWN
//PAST
//FUTURE
//TODAY
function PastTodayFuture(Str: AnsiString): AnsiString;
var I,T: UInt64;
    Format: TFormatSettings;
begin
  Result:='UNKNOWN';
  try
    if Length(Str)=19 then
    begin
      GetLocaleFormatSettings(GetThreadLocale, Format);
      Format.DateSeparator:='-';
      Format.ShortDateFormat:='yyyy-mm-dd';
      Format.TimeSeparator:=':';
      Format.LongTimeFormat:='hh:nn:ss';
      I:=DateTimeToUnix(StrToDatetime(Str,Format));
      T:=DateTimeToUnix(Now);
      if T>I then
      begin
        Result:='PAST';
      end
      else
      begin
        if T<I then
        begin
          Result:='FUTURE';
        end
        else
        begin
          if T=I then
          begin
            Result:='TODAY';
          end;
        end;
      end;
    end;
  except
    Result:='UNKNOWN';
  end;
end;

procedure AddCalendar(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgeteconomiccalendar'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgeteconomiccalendar'].Length-1 do
        begin
          Str:=Obj.A['response.widgeteconomiccalendar'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['currency']+', ';
          //Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['country']+', ';
          if Obj.A['response.widgeteconomiccalendar'].O[n].B['holiday']=True then
          begin
            Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['desc']+' - HOLIDAY, ';
          end
          else
          begin
            Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['desc']+', ';
          end;
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['act']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['fore']+', ';
          Str:=Str+Obj.A['response.widgeteconomiccalendar'].O[n].S['prev']+' ';
          Wig:=MySql.Post('http://mysql.com/insert/economic_calendar',Obj.A['response.widgeteconomiccalendar'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgeteconomiccalendar'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgeteconomiccalendar'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddBank(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetcentralbankrate'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetcentralbankrate'].Length-1 do
        begin
          Str:=Obj.A['response.widgetcentralbankrate'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['actual']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['forecast']+', ';
          Str:=Str+Obj.A['response.widgetcentralbankrate'].O[n].S['previous']+' ';
          Wig:=MySql.Post('http://mysql.com/insert/central_banks_rates',Obj.A['response.widgetcentralbankrate'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetcentralbankrate'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetcentralbankrate'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddSummary(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetfinancialsummary'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetfinancialsummary'].Length-1 do
        begin
          Str:=Obj.A['response.widgetfinancialsummary'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['grossmargin']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['operatingmargin']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['netprofitmargin']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['returnoninvestment']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['quickratio']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['currentratio']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['ltdebttoequity']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['totaldebttoequity']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['cashflowshare']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['revenueshare']+', ';
          Str:=Str+Obj.A['response.widgetfinancialsummary'].O[n].S['operatingcashflow'];
          Wig:=MySql.Post('http://mysql.com/insert/financial_summary',Obj.A['response.widgetfinancialsummary'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetfinancialsummary'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetfinancialsummary'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddSummaryStatement(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetsummaryincomestatement'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetsummaryincomestatement'].Length-1 do
        begin
          Str:=Obj.A['response.widgetsummaryincomestatement'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['totalrevenue']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['grossprofit']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['operatingincome']+', ';
          Str:=Str+Obj.A['response.widgetsummaryincomestatement'].O[n].S['netincome'];
          Wig:=MySql.Post('http://mysql.com/insert/summaryincomestatement',Obj.A['response.widgetsummaryincomestatement'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetsummaryincomestatement'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetsummaryincomestatement'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddSummaryBalanceSheet(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetsummarybalancesheet'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetsummarybalancesheet'].Length-1 do
        begin
          Str:=Obj.A['response.widgetsummarybalancesheet'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['totalassets']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['totalliabilities']+', ';
          Str:=Str+Obj.A['response.widgetsummarybalancesheet'].O[n].S['totalequity'];
          Wig:=MySql.Post('http://mysql.com/insert/summarybalancesheet',Obj.A['response.widgetsummarybalancesheet'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetsummarybalancesheet'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetsummarybalancesheet'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddBalanceSheet(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetbalancesheet'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetbalancesheet'].Length-1 do
        begin
          Str:=Obj.A['response.widgetbalancesheet'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['nm']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['pe']+', ';
          Str:=Str+Obj.A['response.widgetbalancesheet'].O[n].S['vl'];
          Wig:=MySql.Post('http://mysql.com/insert/balancesheet',Obj.A['response.widgetbalancesheet'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetbalancesheet'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetbalancesheet'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddIncomeStatement(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetincomestatement'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetincomestatement'].Length-1 do
        begin
          Str:=Obj.A['response.widgetincomestatement'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['nm']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['pe']+', ';
          Str:=Str+Obj.A['response.widgetincomestatement'].O[n].S['vl'];
          Wig:=MySql.Post('http://mysql.com/insert/incomestatement',Obj.A['response.widgetincomestatement'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetincomestatement'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetincomestatement'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddCash(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetcashstatement'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetcashstatement'].Length-1 do
        begin
          Str:=Obj.A['response.widgetcashstatement'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['cashfromoperating']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['cashfrominvesting']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['cashfromfinancing']+', ';
          Str:=Str+Obj.A['response.widgetcashstatement'].O[n].S['cashnetchangein'];
          Wig:=MySql.Post('http://mysql.com/insert/cashflowstatement',Obj.A['response.widgetcashstatement'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetcashstatement'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetcashstatement'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddDividents(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetdividends'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetdividends'].Length-1 do
        begin
          Str:=Obj.A['response.widgetdividends'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['dividend']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['type']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['mark']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['paymentdate']+', ';
          Str:=Str+Obj.A['response.widgetdividends'].O[n].S['yield'];
          Wig:=MySql.Post('http://mysql.com/insert/dividents',Obj.A['response.widgetdividends'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetdividends'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetdividends'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddRation(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetration'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetration'].Length-1 do
        begin
          Str:=Obj.A['response.widgetration'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['name']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['mark']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['company']+', ';
          Str:=Str+Obj.A['response.widgetration'].O[n].S['industry'];
          Wig:=MySql.Post('http://mysql.com/insert/ration',Obj.A['response.widgetration'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetration'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetration'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddEarning(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetearning'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetearning'].Length-1 do
        begin
          Str:=Obj.A['response.widgetearning'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['eps']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['epsforecast']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['revenue']+', ';
          Str:=Str+Obj.A['response.widgetearning'].O[n].S['revenueforecast'];
          Wig:=MySql.Post('http://mysql.com/insert/earning',Obj.A['response.widgetearning'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetearning'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetearning'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

procedure AddCashflow(Obj: ISuperObject);
var Wig: ISuperObject;
    Str: AnsiString;
    n: UInt64;
begin
  if Assigned(Obj) then
  begin
    if Obj.B['status']=True then
    begin
      if Obj.A['response.widgetcashflow'].Length>0 then
      begin
        for n:=0 to Obj.A['response.widgetcashflow'].Length-1 do
        begin
          Str:=Obj.A['response.widgetcashflow'].O[n].S['timestamp']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['hash']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['event']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['time']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['currency']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['desc']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['title']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['nm']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['pe']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['le']+', ';
          Str:=Str+Obj.A['response.widgetcashflow'].O[n].S['vl'];
          Wig:=MySql.Post('http://mysql.com/insert/cashflow',Obj.A['response.widgetcashflow'].O[n]);
          if Wig.B['status']=True then
          begin
            writeln('[ OK ] '+PastTodayFuture(Obj.A['response.widgetcashflow'].O[n].S['timestamp'])+#09+Str);
          end
          else
          begin
            writeln('[ ER ] '+PastTodayFuture(Obj.A['response.widgetcashflow'].O[n].S['timestamp'])+#09+Str);
          end;
        end;
      end
      else
      begin
        writeln('[ ER ] '+IntToStr(Obj.I['code'])+' Empty');
      end;
    end
    else
    begin
      writeln('[ ER ] '+IntToStr(Obj.I['code'])+' '+Obj.S['description']);
    end;
  end
  else
  begin
    writeln('[ ER ] Object not found');
  end;
end;

begin

////
///  NOT START AND NOT USE FOR TEST THIS APPLICATION!!
///
///
///
///  NOT START AND NOT USE FOR TEST THIS APPLICATION!!
///
///
///
///  NOT START AND NOT USE FOR TEST THIS APPLICATION!!
///


  MySql:=TPushMysqlDatabase.Create;
  MySql.Active:=True;
  if MySql.Active then
  begin
    writeln('[ OK ] Connected to database '+ Mysql.Address);
  end
  else
  begin
    writeln('[ ER ] Not connected to database '+ Mysql.Address+' '+IntToStr(Mysql.Port)+' '+Mysql.Login+' '+Mysql.Password);
  end;

  Investing:=TScrapInvestment.Create;
  Investing.Active:=True;
  if Investing.Active then writeln('[ OK ] Connected to Investment') else  writeln('[ ER ] Not connected to Investment');
  writeln('[ IF ] Datetime format: yyyy-mm-dd hh:nn:ss');


    //AddCalendar(Investing.Get('https://api.investment.com/query/economiccalendar?from=2022-07-01&to=2022-07-30'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=FED'));
    //readln;



    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=ECB'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=RBA'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=BOJ'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=BOC'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=SNB'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=RBNZ'));
    //AddBank(Investing.Get('https://api.investment.com/query/centralbankrates?name=BOE'));

    //AddSummary(Investing.Get('https://api.investment.com/query/financialsummary?name=MMM'));
    //AddSummary(Investing.Get('https://api.investment.com/query/financialsummary?name=AXP'));
    //AddSummary(Investing.Get('https://api.investment.com/query/financialsummary?name=AAPL'));
    //AddSummary(Investing.Get('https://api.investment.com/query/financialsummary?name=CO'));

    //AddSummaryStatement(Investing.Get('https://api.investment.com/query/summaryincomestatement?name=MMM'));
    //AddSummaryStatement(Investing.Get('https://api.investment.com/query/summaryincomestatement?name=AXP'));
    //AddSummaryStatement(Investing.Get('https://api.investment.com/query/summaryincomestatement?name=AAPL'));
    //AddSummaryStatement(Investing.Get('https://api.investment.com/query/summaryincomestatement?name=CO'));

    //AddSummaryBalanceSheet(Investing.Get('https://api.investment.com/query/summarybalancesheet?name=MMM'));
    //AddSummaryBalanceSheet(Investing.Get('https://api.investment.com/query/summarybalancesheet?name=AXP'));
    //AddSummaryBalanceSheet(Investing.Get('https://api.investment.com/query/summarybalancesheet?name=AAPL'));
    //AddSummaryBalanceSheet(Investing.Get('https://api.investment.com/query/summarybalancesheet?name=CO'));

    //AddCash(Investing.Get('https://api.investment.com/query/summarycashflowstatement?name=MMM'));
    //AddCash(Investing.Get('https://api.investment.com/query/summarycashflowstatement?name=AXP'));
    //AddCash(Investing.Get('https://api.investment.com/query/summarycashflowstatement?name=AAPL'));
    //AddCash(Investing.Get('https://api.investment.com/query/summarycashflowstatement?name=CO'));

    //AddDividents(Investing.Get('https://api.investment.com/query/dividents?name=MMM'));
    //AddDividents(Investing.Get('https://api.investment.com/query/dividents?name=AXP'));
    //AddDividents(Investing.Get('https://api.investment.com/query/dividents?name=AAPL'));
    //AddDividents(Investing.Get('https://api.investment.com/query/dividents?name=CO'));

    //AddRation(Investing.Get('https://api.investment.com/query/ration?name=MMM'));
    //AddRation(Investing.Get('https://api.investment.com/query/ration?name=AXP'));
    //AddRation(Investing.Get('https://api.investment.com/query/ration?name=AAPL'));
    //AddRation(Investing.Get('https://api.investment.com/query/ration?name=CO'));

    //AddEarning(Investing.Get('https://api.investment.com/query/earning?name=MMM'));
    //AddEarning(Investing.Get('https://api.investment.com/query/earning?name=AXP'));
    //AddEarning(Investing.Get('https://api.investment.com/query/earning?name=AAPL'));
    //AddEarning(Investing.Get('https://api.investment.com/query/earning?name=CO'));

    //AddCashflow(Investing.Get('https://api.investment.com/query/cashflow?name=MMM'));
    //AddCashflow(Investing.Get('https://api.investment.com/query/cashflow?name=AXP'));
    //AddCashflow(Investing.Get('https://api.investment.com/query/cashflow?name=AAPL'));
    //AddCashflow(Investing.Get('https://api.investment.com/query/cashflow?name=CO'));

    //AddBalanceSheet(Investing.Get('https://api.investment.com/query/balancesheet?name=MMM'));
    //AddBalanceSheet(Investing.Get('https://api.investment.com/query/balancesheet?name=AXP'));
    //AddBalanceSheet(Investing.Get('https://api.investment.com/query/balancesheet?name=AAPL'));
    //AddBalanceSheet(Investing.Get('https://api.investment.com/query/balancesheet?name=CO'));

    //AddIncomeStatement(Investing.Get('https://api.investment.com/query/incomestatement?name=MMM'));
    //AddIncomeStatement(Investing.Get('https://api.investment.com/query/incomestatement?name=AXP'));
    //AddIncomeStatement(Investing.Get('https://api.investment.com/query/incomestatement?name=AAPL'));
    //AddIncomeStatement(Investing.Get('https://api.investment.com/query/incomestatement?name=CO'));

  writeln('STOP');
  readln;

  Investing.Destroy;
  Investing:=nil;
  MySql.Destroy;
  MySql:=nil;
  readln;
end.
